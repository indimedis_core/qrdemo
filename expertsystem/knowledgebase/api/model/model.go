package model

type GeneGraph struct {
	Assemblies []Assembly  `json:"assemblies,omitempty"`
	Sequences  []Sequence  `json:"sequences,omitempty"`
	Snps       []SNP       `json:"snps,omitempty"`
	Haplotypes []Haplotype `json:"haplotypes,omitempty"`
	Genotypes  []Genotype  `json:"genotypes,omitempty"`
	Phenotypes []Phenotype `json:"phenotypes,omitempty"`
}

type Phenotype struct {
	DgraphType []string   `json:"dgraph.type,omitempty"`
	UID        string     `json:"uid,omitempty"`
	ID         string     `json:"phenotype.id,omitempty"`
	Name       string     `json:"phenotype.name,omitempty"`
	Genotype   []Genotype `json:"phenotype.genotype,omitempty"`
}

type Genotype struct {
	DgraphType []string    `json:"dgraph.type,omitempty"`
	UID        string      `json:"uid,omitempty"`
	ID         string      `json:"genotype.id,omitempty"`
	Name       string      `json:"genotype.name,omitempty"`
	Haplotype  []Haplotype `json:"genotype.haplotype,omitempty"`
}

type Haplotype struct {
	DgraphType []string `json:"dgraph.type,omitempty"`
	UID        string   `json:"uid,omitempty"`
	ID         string   `json:"haplotype.id,omitempty"`
	Name       string   `json:"haplotype.name,omitempty"`
	Snp        []SNP    `json:"haplotype.snp,omitempty"`
}

// type Allele struct {
// 	DgraphType []string    `json:"dgraph.type,omitempty"`
// 	UID        string      `json:"uid,omitempty"`
// 	Placements []Placement `json:"allele.placements,omitempty"`
// }

type Assembly struct {
	DgraphType   []string `json:"dgraph.type,omitempty"`
	UID          string   `json:"uid,omitempty"`
	ID           string   `json:"assembly.id,omitempty"`
	Name         string   `json:"assembly.name,omitempty"`
	Accession    string   `json:"assembly.accession,omitempty"`
	IsTopLevel   bool     `json:"assembly.is_top_level,omitempty"`
	IsAlt        bool     `json:"assembly.is_alt,omitempty"`
	IsPatch      bool     `json:"assembly.is_patch,omitempty"`
	IsChromosome bool     `json:"assembly.is_chromosome,omitempty"`
}

type Sequence struct {
	DgraphType []string `json:"dgraph.type,omitempty"`
	UID        string   `json:"uid,omitempty"`
	ID         string   `json:"sequence.id,omitempty"`
	Name       string   `json:"sequence.name,omitempty"`
	Assembly   Assembly `json:"sequence.assembly,omitempty"`
}

type SNPDefinition struct {
	DgraphType []string `json:"dgraph.type,omitempty"`
	UID        string   `json:"uid,omitempty"`
	Name       string   `json:"snp_definition.name,omitempty"`
	Sequence   Sequence `json:"snp_definition.sequence,omitempty"`
	Position   int      `json:"snp_definition.position,omitempty"`
	Ref        string   `json:"snp_definition.reference,omitempty"`
	Alt        string   `json:"snp_definition.alternation,omitempty"`
	HGSV       string   `json:"snp_definition.hgsv,omitempty"`
}

type GeneticTest struct {
	DgraphType []string `json:"dgraph.type,omitempty"`
	UID        string   `json:"uid,omitempty"`
	ID         string   `json:"genetic_test.id,omitempty"`
	Name       string   `json:"genetic_test.name,omitempty"`
	Snps       []SNP    `json:"genetic_test.snp,omitempty"`
}

type SNP struct {
	DgraphType  []string        `json:"dgraph.type"`
	UID         string          `json:"uid,omitempty"`
	SnpID       string          `json:"snp.id,omitempty"`
	Name        string          `json:"snp.name,omitempty"`
	Definitions []SNPDefinition `json:"snp.definition,omitempty"`
	Frequency   float32         `json:"snp.frequency,omitempty"`
}

type Guideline struct {
	DgraphType []string
	UID        string
	Alerts     []Alert
	SNPs       []SNP
}

type Alert struct {
	AssociatedPhenotypes []Phenotype
}

type Gene struct {
	Phenotypes []Phenotype
}

type Drug struct {
	Guidelines []Guideline
}
