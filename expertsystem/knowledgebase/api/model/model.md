# Graph model

```graphviz
digraph finite_state_machine {
    rankdir=LR;


    node [shape = circle];
    Assembly -> Sequence [ label = "assembly.sequence" ];
    Sequence  -> SNPDefinition [ label = "sequence.snp_definition" ];
    SNP  -> SNPDefinition  [ label = "snp.snp_definition" ];
    Haplotype -> SNP  [ label = "haplotype.snp" ];
    Genotype -> Haplotype [ label = "genotype.haplotype" ];
    Phenotype -> Genotype [ label = "phenotype.haplotype" ];

    Guideline -> Phenotype [ label = "guideline.phenotype" ];
    Guideline -> SNP [ label = "guideline.snp" ];
    Guideline -> Haplotype [ label = "guideline.haplotype" ];
}
```
