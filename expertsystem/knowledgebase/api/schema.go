package genegraph

var Schema = predicates + types

var predicates = `
assembly.id: string @index(hash) .
assembly.name: string @index(hash) .
assembly.accession: string @index(hash) .
assembly.is_top_level: bool .
assembly.is_alt: bool .
assembly.is_patch: bool .
assembly.is_chromosome: bool .

sequence.id: string @index(hash) .
sequence.name: string  @index(hash) .
sequence.assembly: uid @reverse .

snp.id: string @index(hash) .
snp.definition: [uid] @reverse .

snp_definition.sequence: uid @reverse .
snp_definition.position: int @index(int).
snp_definition.reference: string @index(hash) .
snp_definition.alternation: string @index(hash) .
snp_definition.hgsv: string  @index(hash) .

haplotype.id: string @index(hash) .
haplotype.name: string @index(hash) .
haplotype.snp: [uid] @reverse .

genotype.id: string @index(hash) .
genotype.name: string @index(hash) .
genotype.haplotype: [uid] @reverse .

phenotype.name: string @index(hash) .
phenotype.id: string @index(hash) .
phenotype.genotype: [uid] @reverse .
`

var types = `
type Assembly {
	assembly.id: string
	assembly.name: string
	assembly.accession: string
	assembly.is_top_level: bool
	assembly.is_alt: bool
	assembly.is_patch: bool
	assembly.is_chromosome: bool
	
}

type Sequence {
	sequence.id: string
	sequence.name: string
	sequence.assembly: Assembly
}

type SNP {
	snp.id: string
	snp.definition: [SNPDefinition]
}

type SNPDefinition {
	snp_definition.sequence: Sequence
	snp_definition.position: int
	snp_definition.reference: string
	snp_definition.alternation: string
	snp_definition.hgsv: string
}

type Haplotype {
	haplotype.name: string
	haplotype.id: string
	haplotype.snp: [SNP]
}

type Genotype {
	genotype.id: string 
	genotype.name: string 
	genotype.haplotype: [Haplotype]
}

type Phenotype {
	henotype.name: string 
	phenotype.id: string 
	phenotype.genotype: [Genotype]
}

`
