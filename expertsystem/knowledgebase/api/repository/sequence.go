package repository

import (
	"context"
	"encoding/json"
	"log"

	"github.com/dgraph-io/dgo/v2/protos/api"
	"indimedis.com/fhir/app/genegraph/model"
	"indimedis.com/fhir/pkg/dgraph"
)

type SequenceRepository struct {
	db dgraph.Client
	// json jsoniter.API
}

func NewSequenceRepo(client dgraph.Client) *SequenceRepository {
	// , jsonApi jsoniter.API
	return &SequenceRepository{
		db: client,
		// json: jsonApi,
	}
}

func (r *SequenceRepository) Create(p model.Sequence) {
	conn, closeFunc := r.db.Open()
	pb, err := json.MarshalIndent(p, "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(pb))

	txn := conn.NewTxn()
	mu := &api.Mutation{
		SetJson: pb,
	}
	resp, err := txn.Mutate(context.Background(), mu)
	if err != nil {
		log.Fatal(err, resp)
	}
	err = txn.Commit(context.Background())
	if err != nil {
		log.Fatal(err, resp)
	}
	log.Println(resp.String())

	closeFunc()
}
