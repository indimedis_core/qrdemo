package repository_test

import (
	"testing"

	"indimedis.com/fhir/app/genegraph/repository"
)

// func TestNewRepository(t *testing.T) {

// 	repo := patient.NewRepository(dClient)
// 	if repo == nil {
// 		t.Errorf("repo is %s", repo)
// 	}
// }

func TestCreateAssembly(t *testing.T) {
	repo := repository.NewAssemblyRepo(dClient)

	for _, v := range data.Assemblies {
		repo.Create(v)
	}

	// cleanGraphDB()
}

// func TestGetPatient(t *testing.T) {
// 	repo := patient.NewRepository(dClient)
// 	for _, v := range modelPatients.Patients {
// 		repo.Create(v)
// 		p, err := repo.Get(*v.ID)
// 		if err != nil {
// 			t.Error(err)
// 		}
// 		fmt.Println("p:", p)
// 		fmt.Println("V:", v)

// 	}
// 	cleanGraphDB()
// }
