package genegraph

import (
	"flag"
	"os"
	"strings"

	"github.com/fsnotify/fsnotify"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

const (
	// used for various pathes etc.
	varApplicationName = "fhirserver"
	// Constants for viper variable names. Will be used to set
	// default values as well as to get each value
	varLogLevel                = "log.level"
	varConfigFilePath          = "config.path"
	varSystemImage             = "system.image"
	varSystemImageTag          = "system.imageTag"
	varServicePort             = "service.port"
	varKafkaObservationBrokers = "kafka.observation.brokers"
	varKafkaObservationTopic   = "kafka.observation.topic"
	varKafkaObservationGroupID = "kafka.observation.groupID"
	varKafkaPatientBrokers     = "kafka.patient.brokers"
	varKafkaPatientTopic       = "kafka.patient.topic"
	varKafkaPatientGroupID     = "kafka.patient.groupID"
	varDgraphServers           = "dgraphServers"
)

type Configuration struct {
	v *viper.Viper
}

var TestConfigPath = "./testdata/conf/fhirgraph"

func NewConfiguration() *Configuration {
	c := Configuration{
		v: viper.New(),
	}
	setDefaults(&c)

	c.v.AutomaticEnv()
	c.v.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	c.v.SetTypeByDefaultValue(true)

	configpath := flag.String("config", "", "path to a yaml configuration file")
	flag.Parse()
	logrus.Warn(*configpath)
	if configpath != nil && *configpath != "" {
		c.v.SetConfigFile(*configpath)
	}

	logrus.WithField("path", c.GetConfigFilePath()).Info("loading config")

	if err := c.v.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			// Config file not found; ignore error if desired
			logrus.Warnf("no config file '%s' not found. Using default values", c.GetConfigFilePath())
			return &c
		}
		// Config file was found but another error was produced
		logrus.Fatalf("fatal error while reading the config file: %s", err)
	}

	// counter := 0
	// for err != nil {
	// 	if counter > 10 {
	// 		logrus.Fatalf("No configuration file found at paths: %s", err)
	// 	}
	// 	// just use the default value(s) if the config file was not found
	// 	logrus.Errorf("Error while reading config file: %s, %s sleeping 5s", reflect.TypeOf(err), err)
	// 	time.Sleep(5 * time.Second)
	// 	err = c.v.ReadInConfig() // Find and read the config file
	// 	logrus.WithField("path", c.GetConfigFilePath()).Warn("loading config")
	// }
	setLogLevel(c.GetLogLevel())
	// monitor the changes in the config file
	c.v.WatchConfig()
	c.v.OnConfigChange(func(e fsnotify.Event) {
		logrus.WithField("file", e.Name).Warn("Config file changed")
		setLogLevel(c.GetLogLevel())
	})
	return &c
}

func setDefaults(c *Configuration) {
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
	})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	logrus.SetOutput(os.Stdout)

	// Only log the warning severity or above.
	logrus.SetLevel(logrus.WarnLevel)

	c.v.SetDefault(varLogLevel, "info")
	c.v.SetDefault(varServicePort, "8080")
	c.v.SetDefault(varSystemImage, "")
	c.v.SetDefault(varSystemImageTag, "")
	c.v.SetDefault(varKafkaObservationBrokers, []string{"localhost:9092"})
	c.v.SetDefault(varKafkaObservationTopic, "fhir-observation")
	c.v.SetDefault(varKafkaObservationGroupID, "fhirgraph")
	c.v.SetDefault(varKafkaPatientBrokers, []string{"localhost:9092"})
	c.v.SetDefault(varKafkaPatientTopic, "fhir-patient")
	c.v.SetDefault(varKafkaPatientGroupID, "fhirgraph")
	c.v.SetDefault(varDgraphServers, []string{"localhost:9080"})

	// Add default config pathes
	c.v.AddConfigPath("/opt/" + varApplicationName + "/")
	c.v.AddConfigPath("/var/lib/" + varApplicationName + "/")
	c.v.AddConfigPath("./")
	c.v.AddConfigPath("$HOME/" + varApplicationName + "/")

	if TestConfigPath != "" {
		c.v.AddConfigPath(TestConfigPath)
	}

	c.v.SetConfigName("config")
}

func setLogLevel(level string) {
	logrus.Info("Setting log level to " + level)
	switch level {
	case "trace":
		logrus.SetLevel(logrus.TraceLevel)
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
		break
	case "info":
		logrus.SetLevel(logrus.InfoLevel)
	case "warn":
		logrus.SetLevel(logrus.WarnLevel)
		break
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
		break
	case "fatal":
		logrus.SetLevel(logrus.FatalLevel)
		break
	case "panic":
		logrus.SetLevel(logrus.PanicLevel)
		break
	default:
		logrus.SetLevel(logrus.WarnLevel)
	}
}

// GetLogLevel returns the log level
func (c *Configuration) GetLogLevel() string {
	return c.v.GetString(varLogLevel)
}

// GetPathToConfig returns the log level
func (c *Configuration) GetConfigFilePath() string {
	return c.v.ConfigFileUsed()
}
func (c *Configuration) GetSystemImage() string {
	return c.v.GetString(varSystemImage)
}
func (c *Configuration) GetSystemImageTag() string {
	return c.v.GetString(varSystemImageTag)
}
func (c *Configuration) GetServerPort() string {
	return c.v.GetString(varServicePort)
}
func (c *Configuration) GetKafkaObservationBrokers() []string {
	return c.v.GetStringSlice(varKafkaObservationBrokers)
}
func (c *Configuration) GetKafkaObservationTopic() string {
	return c.v.GetString(varKafkaObservationTopic)
}
func (c *Configuration) GetKafkaObservationGroupID() string {
	return c.v.GetString(varKafkaObservationGroupID)
}
func (c *Configuration) GetKafkaPatientBrokers() []string {
	return c.v.GetStringSlice(varKafkaPatientBrokers)
}
func (c *Configuration) GetKafkaPatientTopic() string {
	return c.v.GetString(varKafkaPatientTopic)
}
func (c *Configuration) GetKafkaPatientGroupID() string {
	return c.v.GetString(varKafkaPatientGroupID)
}
func (c *Configuration) GetDgraphServers() []string {
	return c.v.GetStringSlice(varDgraphServers)
}
func (c *Configuration) Print() {
	c.v.Debug()
}
