package com.indimedis.knowledgebase;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class GreetingControllerTests {

	@Autowired
	GreetingController controller;

	@Test
	@Timeout(60)
	void contextLoads() throws Exception {
		assertThat(controller).isNotNull();
	}

}
