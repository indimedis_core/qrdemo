package com.indimedis.knowledgebase;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.janusgraph.core.Cardinality;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.Multiplicity;
import org.janusgraph.core.schema.JanusGraphManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(0)
public class DemoApplicationListner implements ApplicationListener<ApplicationReadyEvent> {

  private static final Logger logger = LoggerFactory.getLogger(DemoApplicationListner.class);

  @Autowired
  JanusGraph janusGraph;

  @Autowired
  GraphTraversalSource g;

  @Override
  public void onApplicationEvent(ApplicationReadyEvent event) {
    JanusGraphManagement mgmt = janusGraph.openManagement();
    createSchema(mgmt);
    logger.info("Schema created");
    addData(g);
    logger.info("Data added");
    var vertex = g.V().toList();
    logger.info(Integer.toString(vertex.size()));
    
    // Vertex v =
    // g.addV().property(Cardinality.list,"name","marko").property("name","marko a.
    // rodriguez").next();
    // logger.info(Long.toString(g.V(v).properties("name").count().next()));
  }

  private void addData(GraphTraversalSource g) {
    // GRCh38.p12
    g.tx().open();
    var grch38_p12 = g.addV("assembly").property("id", "grch38p12").property("name", "GRCh38.p12")
        .property("accession", "GCF_000001405.38").property("is_top_level", true).property("is_alt", false)
        .property("is_patch", false).property("is_chromosome", true).next();

    // GRCh37.p13
    var grch37_p13 = g.addV("assembly").property("id", "grch37p13").property("name", "GRCh37.p13")
        .property("accession", "GCF_000001405.25").property("is_top_level", true).property("is_alt", false)
        .property("is_patch", false).property("is_chromosome", true).next();

    // NC_000010.10
    var nc_000010_10 = g.addV("sequence").property("id", "NC_000010_10").property("name", "NC_000010.10")
        .addE("assembly").to(grch37_p13).next();

    /** NC_000010_11 */
    var nc_000010_11 = g.addV("sequence").property("id", "NC_000010_11").property("name", "NC_000010.11").next();

    g.V(nc_000010_11).addE("assembly").to(grch38_p12).next();

    var nc_000001_11 = g.addV("sequence").property("id", "NC_000001_11").property("name", "NC_000001.11").next();

    g.V(nc_000001_11).addE("assembly").to(grch38_p12).next();

    var nc_000002_12 = g.addV("sequence").property("id", "NC_000002_12").property("name", "NC_000002.12").next();
    g.V(nc_000002_12).addE("assembly").to(grch38_p12).next();

    // NC_000010.11:g.94852914A>C
    var g94852914A_C = g.addV("snp_definition").property("name", "NC_000010.11:g.94852914A>C")
        .property("snp_position", 94852914).property("ref_allele", "A").property("alt_allele", "C").next();
    g.V(g94852914A_C).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94852914A_C = g.addV("snp").property("id", "NC_000010_11_g_94852914A_C").next();
    g.V(nc_000010_11_g_94852914A_C).addE("snp_definition").to(g94852914A_C).next();

    // NC_000010.11:g.94852785C>G
    var g94852785C_G = g.addV("snp_definition").property("name", "NC_000010.11:g.94852785C>G")
        .property("snp_position", 94852785).property("ref_allele", "C").property("alt_allele", "G").next();
    g.V(g94852785C_G).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94852785C_G = g.addV("snp").property("id", "NC_000010_11_g_94852785C_G").next();
    g.V(nc_000010_11_g_94852785C_G).addE("snp_definition").to(g94852785C_G).next();

    // NC_000010.11:g.94852738C>T
    var g94852738C_T = g.addV("snp_definition").property("name", "NC_000010.11:g.94852738C>T")
        .property("snp_position", 94852738).property("ref_allele", "C").property("alt_allele", "T").next();
    g.V(g94852738C_T).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94852738C_T = g.addV("snp").property("id", "NC_000010_11_g_94852738C_T").next();
    g.V(nc_000010_11_g_94852738C_T).addE("snp_definition").to(g94852738C_T).next();

    // NC_000010.11:g.94849995C>T
    var g94849995C_T = g.addV("snp_definition").property("name", "NC_000010.11:g.94849995C>T")
        .property("snp_position", 94849995).property("ref_allele", "C").property("alt_allele", "T").next();
    g.V(g94849995C_T).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94849995C_T = g.addV("snp").property("id", "NC_000010_11_g_94849995C_T").next();
    g.V(nc_000010_11_g_94849995C_T).addE("snp_definition").to(g94849995C_T).next();

    // NC_000010.11:g.94842995G>A
    var g94842995G_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94842995G>A")
        .property("snp_position", 94842995).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g94842995G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94842995G_A = g.addV("snp").property("id", "NC_000010_11_g_94842995G_A").next();
    g.V(nc_000010_11_g_94842995G_A).addE("snp_definition").to(g94842995G_A).next();

    // NC_000010.11:g.94842879G>A
    var g94842879G_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94842879G>A")
        .property("snp_position", 94842879).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g94842879G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94842879G_A = g.addV("snp").property("id", "NC_000010_11_g_94842879G_A").next();
    g.V(nc_000010_11_g_94842879G_A).addE("snp_definition").to(g94842879G_A).next();

    // NC_000010.11:g.94842866A>G
    var g94842866A_G = g.addV("snp_definition").property("name", "NC_000010.11:g.94842866A>G")
        .property("snp_position", 94842866).property("ref_allele", "A").property("alt_allele", "G").next();
    g.V(g94842866A_G).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94842866A_G = g.addV("snp").property("id", "NC_000010_11_g_94842866A_G").next();
    g.V(nc_000010_11_g_94842866A_G).addE("snp_definition").to(g94842866A_G).next();

    // NC_000010.11:g.94842861G>A
    var g94842861G_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94842861G>A")
        .property("snp_position", 94842861).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g94842861G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94842861G_A = g.addV("snp").property("id", "NC_000010_11_g_94842861G_A").next();
    g.V(nc_000010_11_g_94842861G_A).addE("snp_definition").to(g94842861G_A).next();

    // NC_000010.11:g.94781999T>A
    var g94781999T_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94781999T>A")
        .property("snp_position", 94781999).property("ref_allele", "T").property("alt_allele", "A").next();
    g.V(g94781999T_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94781999T_A = g.addV("snp").property("id", "NC_000010_11_g_94781999T_A").next();
    g.V(nc_000010_11_g_94781999T_A).addE("snp_definition").to(g94781999T_A).next();

    // NC_000010.11:g.94781944G>A
    var g94781944G_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94781944G>A")
        .property("snp_position", 94781944).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g94781944G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94781944G_A = g.addV("snp").property("id", "NC_000010_11_g_94781944G_A").next();
    g.V(nc_000010_11_g_94781944G_A).addE("snp_definition").to(g94781944G_A).next();

    // NC_000010.11:g.94781859G>A
    var g94781859G_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94781859G>A")
        .property("snp_position", 94781859).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g94781859G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94781859G_A = g.addV("snp").property("id", "NC_000010_11_g_94781859G_A").next();
    g.V(nc_000010_11_g_94781859G_A).addE("snp_definition").to(g94781859G_A).next();

    // NC_000010.11:g.94761900C>T
    var g94761900C_T = g.addV("snp_definition").property("name", "NC_000010.11:g.94761900C>T")
        .property("snp_position", 94761900).property("ref_allele", "C").property("alt_allele", "T").next();
    g.V(g94761900C_T).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94761900C_T = g.addV("snp").property("id", "NC_000010_11_g_94761900C_T").next();
    g.V(nc_000010_11_g_94761900C_T).addE("snp_definition").to(g94761900C_T).next();

    // NC_000010.11:g.94762712C>T
    var g94762712C_T = g.addV("snp_definition").property("name", "NC_000010.11:g.94762712C>T")
        .property("snp_position", 94762712).property("ref_allele", "C").property("alt_allele", "T").next();
    g.V(g94762712C_T).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94762712C_T = g.addV("snp").property("id", "NC_000010_11_g_94762712C_T").next();
    g.V(nc_000010_11_g_94762712C_T).addE("snp_definition").to(g94762712C_T).next();

    // NC_000010.11:g.94762715T>C
    var g94762715T_C = g.addV("snp_definition").property("name", "NC_000010.11:g.94762715T>C")
        .property("snp_position", 94762715).property("ref_allele", "T").property("alt_allele", "C").next();
    g.V(g94762715T_C).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94762715T_C = g.addV("snp").property("id", "NC_000010_11_g_94762715T_C").next();
    g.V(nc_000010_11_g_94762715T_C).addE("snp_definition").to(g94762715T_C).next();

    // NC_000010.11:g.94762755T>C
    var g94762755T_C = g.addV("snp_definition").property("name", "NC_000010.11:g.94762755T>C")
        .property("snp_position", 94762715).property("ref_allele", "T").property("alt_allele", "C").next();
    g.V(g94762755T_C).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94762755T_C = g.addV("snp").property("id", "NC_000010_11_g_94762755T_C").next();
    g.V(nc_000010_11_g_94762755T_C).addE("snp_definition").to(g94762755T_C).next();

    // NC_000010.11:g.94762760A>C
    var g94762760A_C = g.addV("snp_definition").property("name", "NC_000010.11:g.94762760A>C")
        .property("snp_position", 94762760).property("ref_allele", "A").property("alt_allele", "C").next();
    g.V(g94762760A_C).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94762760A_C = g.addV("snp").property("id", "NC_000010_11_g_94762760A_C").next();
    g.V(nc_000010_11_g_94762760A_C).addE("snp_definition").to(g94762760A_C).next();

    // NC_000010.11:g.94762788A>T
    var g94762788A_T = g.addV("snp_definition").property("name", "NC_000010.11:g.94762788A>T")
        .property("snp_position", 94762788).property("ref_allele", "A").property("alt_allele", "T").next();
    g.V(g94762788A_T).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94762788A_T = g.addV("snp").property("id", "NC_000010_11_g_94762788A_T").next();
    g.V(nc_000010_11_g_94762788A_T).addE("snp_definition").to(g94762788A_T).next();

    // NC_000010.11:g.94762856A>G
    var g94762856A_G = g.addV("snp_definition").property("name", "NC_000010.11:g.94762856A>G")
        .property("snp_position", 94762856).property("ref_allele", "A").property("alt_allele", "G").next();
    g.V(g94762856A_G).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94762856A_G = g.addV("snp").property("id", "NC_000010_11_g_94762856A_G").next();
    g.V(nc_000010_11_g_94762856A_G).addE("snp_definition").to(g94762856A_G).next();

    // NC_000010.11:g.94775106C>T
    var g94775106C_T = g.addV("snp_definition").property("name", "NC_000010.11:g.94775106C>T")
        .property("snp_position", 94775106).property("ref_allele", "C").property("alt_allele", "T").next();
    g.V(g94775106C_T).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94775106C_T = g.addV("snp").property("id", "NC_000010_11_g_94775106C_T").next();
    g.V(nc_000010_11_g_94775106C_T).addE("snp_definition").to(g94775106C_T).next();

    // NC_000010.11:g.94775121C>T
    var g94775121C_T = g.addV("snp_definition").property("name", "NC_000010.11:g.94775121C>T")
        .property("snp_position", 94775121).property("ref_allele", "C").property("alt_allele", "T").next();
    g.V(g94775121C_T).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94775121C_T = g.addV("snp").property("id", "NC_000010_11_g_94775121C_T").next();
    g.V(nc_000010_11_g_94775121C_T).addE("snp_definition").to(g94775121C_T).next();

    // NC_000010.11:g.94775160G>C
    var g94775160G_C = g.addV("snp_definition").property("name", "NC_000010.11:g.94775160G>C")
        .property("snp_position", 94775160).property("ref_allele", "G").property("alt_allele", "C").next();
    g.V(g94775160G_C).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94775160G_C = g.addV("snp").property("id", "NC_000010_11_g_94775160G_C").next();
    g.V(nc_000010_11_g_94775160G_C).addE("snp_definition").to(g94775160G_C).next();

    // NC_000010.11:g.94775185A>G
    var g94775185A_G = g.addV("snp_definition").property("name", "NC_000010.11:g.94775185A>G")
        .property("snp_position", 94775185).property("ref_allele", "A").property("alt_allele", "G").next();
    g.V(g94775185A_G).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94775185A_G = g.addV("snp").property("id", "NC_000010_11_g_94775185A_G").next();
    g.V(nc_000010_11_g_94775185A_G).addE("snp_definition").to(g94775185A_G).next();

    // NC_000010.11:g.94775185A>G
    var g94775367A_G = g.addV("snp_definition").property("name", "NC_000010.11:g.94775367A>G")
        .property("snp_position", 94775367).property("ref_allele", "A").property("alt_allele", "G").next();
    g.V(g94775367A_G).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94775367A_G = g.addV("snp").property("id", "NC_000010_11_g_94775367A_G").next();
    g.V(nc_000010_11_g_94775367A_G).addE("snp_definition").to(g94775367A_G).next();

    // NC_000010.11:g.94775416T>C
    var g94775416T_C = g.addV("snp_definition").property("name", "NC_000010.11:g.94775416T>C")
        .property("snp_position", 94775416).property("ref_allele", "T").property("alt_allele", "C").next();
    g.V(g94775416T_C).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94775416T_C = g.addV("snp").property("id", "NC_000010_11_g_94775416T_C").next();
    g.V(nc_000010_11_g_94775416T_C).addE("snp_definition").to(g94775416T_C).next();

    // NC_000010.11:g.94775453G>A
    var g94775453G_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94775453G>A")
        .property("snp_position", 94775453).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g94775453G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94775453G_A = g.addV("snp").property("id", "NC_000010_11_g_94775453G_A").next();
    g.V(nc_000010_11_g_94775453G_A).addE("snp_definition").to(g94775453G_A).next();

    // NC_000010.11:g.94775489G>A
    var g94775489G_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94775489G>A")
        .property("snp_position", 94775489).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g94775489G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94775489G_A = g.addV("snp").property("id", "NC_000010_11_g_94775489G_A").next();
    g.V(nc_000010_11_g_94775489G_A).addE("snp_definition").to(g94775489G_A).next();

    // NC_000010.11:g.94775507G>A
    var g94775507G_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94775507G>A")
        .property("snp_position", 94775507).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g94775507G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94775507G_A = g.addV("snp").property("id", "NC_000010_11_g_94775507G_A").next();
    g.V(nc_000010_11_g_94775507G_A).addE("snp_definition").to(g94775507G_A).next();

    // NC_000010.11:g.94780574G>C
    var g94780574G_C = g.addV("snp_definition").property("name", "NC_000010.11:g.94780574G>C")
        .property("snp_position", 94780574).property("ref_allele", "G").property("alt_allele", "C").next();
    g.V(g94780574G_C).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94780574G_C = g.addV("snp").property("id", "NC_000010_11_g_94780574G_C").next();
    g.V(nc_000010_11_g_94780574G_C).addE("snp_definition").to(g94780574G_C).next();

    // NC_000010.11:g.94780579G>A
    var g94780579G_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94780579G>A")
        .property("snp_position", 94780579).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g94780579G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94780579G_A = g.addV("snp").property("id", "NC_000010_11_g_94780579G_A").next();
    g.V(nc_000010_11_g_94780579G_A).addE("snp_definition").to(g94780579G_A).next();

    // NC_000010.11:g.94780653G>A
    var g94780653G_A = g.addV("snp_definition").property("name", "NC_000010.11:g.94780653G>A")
        .property("snp_position", 94780653).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g94780653G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94780653G_A = g.addV("snp").property("id", "NC_000010_11_g_94780653G_A").next();
    g.V(nc_000010_11_g_94780653G_A).addE("snp_definition").to(g94780653G_A).next();

    // NC_000010.11:g.94781858C>T
    var g94781858C_T = g.addV("snp_definition").property("name", "NC_000010.11:g.94781858C>T")
        .property("snp_position", 94781858).property("ref_allele", "C").property("alt_allele", "T").next();
    g.V(g94781858C_T).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_94781858C_T = g.addV("snp").property("id", "NC_000010_11_g_94781858C_T").next();
    g.V(nc_000010_11_g_94781858C_T).addE("snp_definition").to(g94781858C_T).next();

    // NC_000010.11:g.97573943C>A
    var g97573943C_A = g.addV("snp_definition").property("name", "NC_000010.11:g.97573943C>A")
        .property("snp_position", 97573943).property("ref_allele", "C").property("alt_allele", "A").next();
    g.V(g97573943C_A).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_97573943C_A = g.addV("snp").property("id", "NC_000001_11_g_97573943C_A").next();
    g.V(nc_000010_11_g_97573943C_A).addE("snp_definition").to(g97573943C_A).next();

    // NC_000010.11:g.97515787A_C
    var g97515787A_C = g.addV("snp_definition").property("name", "NC_000010.11:g.97515787A>C")
        .property("snp_position", 97515787).property("ref_allele", "A").property("alt_allele", "C").next();
    g.V(g97515787A_C).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_97515787A_C = g.addV("snp").property("id", "NC_000001_11_g_97515787A_C").next();
    g.V(nc_000010_11_g_97515787A_C).addE("snp_definition").to(g97515787A_C).next();

    // NC_000001.11:g.97450066delG
    var g97450066delG = g.addV("snp_definition").property("name", "NC_000001.11:g.97450066delG")
        .property("snp_position", 97450065).property("ref_allele", "TG").property("alt_allele", "T").next();
    g.V(g97450066delG).addE("sequence").to(nc_000010_11).next();

    var nc_000010_11_g_97450066delG = g.addV("snp").property("id", "NC_000001_11_g_97450066delG").next();
    g.V(nc_000010_11_g_97450066delG).addE("snp_definition").to(g97450066delG).next();

    // NC_000001.11:g.97450058C>T
    var g97450058C_T = g.addV("snp_definition").property("name", "NC_000001.11:g.97450058C>T")
        .property("snp_position", 97450058).property("ref_allele", "C").property("alt_allele", "T").next();
    g.V(g97450058C_T).addE("sequence").to(nc_000010_11).next();

    var nc_000001_11_g_97450058C_T = g.addV("snp").property("id", "NC_000001_11_g_97450058C_T").next();
    g.V(nc_000001_11_g_97450058C_T).addE("snp_definition").to(g97450058C_T).next();

    // NC_000001.11:g.97740415_97740418delATGA
    var g97740415_97740418delATGA = g.addV("snp_definition").property("name", "NC_000001.11:g.97740415_97740418delATGA")
        .property("snp_position", 97740414).property("ref_allele", "AATGA").property("alt_allele", "A").next();
    g.V(g97740415_97740418delATGA).addE("sequence").to(nc_000010_11).next();

    var nc_000001_11_g_97740415_97740418delATGA = g.addV("snp")
        .property("id", "NC_000001_11_g_97740415_97740418delATGA").next();
    g.V(nc_000001_11_g_97740415_97740418delATGA).addE("snp_definition").to(g97740415_97740418delATGA).next();

    // NC_000001.11:g.97079071C>A
    var g97079071C_A = g.addV("snp_definition").property("name", "NC_000001.11:g.97079071C>A")
        .property("snp_position", 97079071).property("ref_allele", "C").property("alt_allele", "A").next();
    g.V(g97079071C_A).addE("sequence").to(nc_000010_11).next();

    var nc_000001_11_g_97079071C_A = g.addV("snp").property("id", "NC_000001_11_g_97079071C_A").next();
    g.V(nc_000001_11_g_97079071C_A).addE("snp_definition").to(g97079071C_A).next();

    // NC_000001.11:g.97691776G>A
    var g97691776G_A = g.addV("snp_definition").property("name", "NC_000001.11:g.97691776G>A")
        .property("snp_position", 97691776).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g97691776G_A).addE("sequence").to(nc_000010_11).next();

    var nc_000001_11_g_97691776G_A = g.addV("snp").property("id", "NC_000001_11_g_97691776G_A").next();
    g.V(nc_000001_11_g_97691776G_A).addE("snp_definition").to(g97691776G_A).next();

    // NC_000001.11:g.97082391T>A
    var g97082391T_A = g.addV("snp_definition").property("name", "NC_000001.11:g.97082391T>A")
        .property("snp_position", 97082391).property("ref_allele", "T").property("alt_allele", "A").next();
    g.V(g97082391T_A).addE("sequence").to(nc_000010_11).next();

    var nc_000001_11_g_97082391T_A = g.addV("snp").property("id", "NC_000001_11_g_97082391T_A").next();
    g.V(nc_000001_11_g_97082391T_A).addE("snp_definition").to(g97082391T_A).next();

    // NC_000001.11:g.97699474T>C
    var g97699474T_C = g.addV("snp_definition").property("name", "NC_000001.11:g.97699474T>C")
        .property("snp_position", 97699474).property("ref_allele", "T").property("alt_allele", "C").next();
    g.V(g97699474T_C).addE("sequence").to(nc_000010_11).next();

    var nc_000001_11_g_97699474T_C = g.addV("snp").property("id", "NC_000001_11_g_97699474T_C").next();
    g.V(nc_000001_11_g_97699474T_C).addE("snp_definition").to(g97699474T_C).next();

    // NC_000001.11:g.97579893G>C
    var g97579893G_C = g.addV("snp_definition").property("name", "NC_000001.11:g.97579893G>C")
        .property("snp_position", 97579893).property("ref_allele", "G").property("alt_allele", "C").next();
    g.V(g97579893G_C).addE("sequence").to(nc_000010_11).next();

    var nc_000001_11_g_97579893G_C = g.addV("snp").property("id", "NC_000001_11_g_97579893G_C").next();
    g.V(nc_000001_11_g_97579893G_C).addE("snp_definition").to(g97579893G_C).next();

    // NC_000002.12:g.233757013T>G
    var g233757013T_G = g.addV("snp_definition").property("name", "NC_000002.12:g.233757013T>G")
        .property("snp_position", 233757013).property("ref_allele", "T").property("alt_allele", "G").next();
    g.V(g233757013T_G).addE("sequence").to(nc_000002_12).next();

    var nc_000002_12_g_233757013T_G = g.addV("snp").property("id", "NC_000002_12_g233757013T_G").next();
    g.V(nc_000002_12_g_233757013T_G).addE("snp_definition").to(g233757013T_G).next();

    // NC_000002.12:g.233759924C>T
    var g233759924C_T = g.addV("snp_definition").property("name", "NC_000002.12:g.233759924C>T")
        .property("snp_position", 233759924).property("ref_allele", "C").property("alt_allele", "T").next();
    g.V(g233759924C_T).addE("sequence").to(nc_000002_12).next();

    var nc_000002_12_g_233759924C_T = g.addV("snp").property("id", "NC_000002_12_g_233759924C_T").next();
    g.V(nc_000002_12_g_233759924C_T).addE("snp_definition").to(g233759924C_T).next();

    // NC_000002.12:g.233760233_233760234insAT
    var g233760233_233760234insAT = g.addV("snp_definition").property("name", "NC_000002.12:g.233760233_233760234insAT")
        .property("snp_position", 233760233).property("ref_allele", "CAT").property("alt_allele", "CATAT").next();
    g.V(g233760233_233760234insAT).addE("sequence").to(nc_000002_12).next();

    var nc_000002_12_g_233760233_233760234insAT = g.addV("snp")
        .property("id", "NC_000002_12_g_233760233_233760234insAT").next();
    g.V(nc_000002_12_g_233760233_233760234insAT).addE("snp_definition").to(g233760233_233760234insAT).next();

    // NC_000002.12:g.233760233_233760234insATAT
    var g233760233_233760234insATAT = g.addV("snp_definition")
        .property("name", "NC_000002.12:g.233760233_233760234insATAT").property("snp_position", 233760233)
        .property("ref_allele", "CAT").property("alt_allele", "CATATAT").next();
    g.V(g233760233_233760234insATAT).addE("sequence").to(nc_000002_12).next();

    var nc_000002_12_g_233760233_233760234insATAT = g.addV("snp")
        .property("id", "NC_000002_12_g_233760233_233760234insATAT").next();
    g.V(nc_000002_12_g_233760233_233760234insATAT).addE("snp_definition").to(g233760233_233760234insATAT).next();

    // NC_000002.12:g.233760234_233760235delAT
    var g233760234_233760235delAT = g.addV("snp_definition").property("name", "NC_000002.12:g.233760234_233760235delAT")
        .property("snp_position", 233760233).property("ref_allele", "CAT").property("alt_allele", "C").next();
    g.V(g233760234_233760235delAT).addE("sequence").to(nc_000002_12).next();

    var nc_000002_12_g_233760234_233760235delAT = g.addV("snp")
        .property("id", "NC_000002_12_g_233760234_233760235delAT").next();
    g.V(nc_000002_12_g_233760234_233760235delAT).addE("snp_definition").to(g233760234_233760235delAT).next();

    // NC_000002.12:g.233760498G>A
    var g233760498G_A = g.addV("snp_definition").property("name", "NC_000002.12:g.233760498G>A")
        .property("snp_position", 233760498).property("ref_allele", "G").property("alt_allele", "A").next();
    g.V(g233760498G_A).addE("sequence").to(nc_000002_12).next();

    var nc_000002_12_g_233760498G_A = g.addV("snp").property("id", "NC_000002_12_g_233760498G_A").next();
    g.V(nc_000002_12_g_233760498G_A).addE("snp_definition").to(g233760498G_A).next();

    // NC_000002.12:g.233760973C>A
    var g233760973C_A = g.addV("snp_definition").property("name", "NC_000002.12:g.233760973C>A")
        .property("snp_position", 233760973).property("ref_allele", "C").property("alt_allele", "A").next();
    g.V(g233760498G_A).addE("sequence").to(nc_000002_12).next();

    var nc_000002_12_g_233760973C_A = g.addV("snp").property("id", "NC_000002_12_g_233760973C_A").next();
    g.V(nc_000002_12_g_233760973C_A).addE("snp_definition").to(g233760973C_A).next();

    var hap_demo_1 = g.addV("haplotype").property("id", "hap_demo_1").property("name", "hap_demo_1").next();
    g.V(hap_demo_1).addE("snps").to(nc_000001_11_g_97079071C_A).next();

    var hap_demo_2 = g.addV("haplotype").property("id", "hap_demo_2").property("name", "hap_demo_2").next();
    g.V(hap_demo_2).addE("snps").to(nc_000001_11_g_97450058C_T).next();

    var hap_demo_3 = g.addV("haplotype").property("id", "hap_demo_3").property("name", "hap_demo_3").next();
    g.V(hap_demo_3).addE("snps").to(nc_000001_11_g_97450058C_T).next();
    g.V(hap_demo_3).addE("snps").to(nc_000001_11_g_97079071C_A).next();

    var hap_demo_4 = g.addV("haplotype").property("id", "hap_demo_4").property("name", "hap_demo_4").next();
    g.V(hap_demo_4).addE("snps").to(nc_000001_11_g_97082391T_A).next();
    g.V(hap_demo_4).addE("snps").to(nc_000001_11_g_97079071C_A).next();

    var hap_demo_5 = g.addV("haplotype").property("id", "hap_demo_5").property("name", "hap_demo_5").next();
    g.V(hap_demo_5).addE("snps").to(nc_000010_11_g_94775121C_T).next();
    g.V(hap_demo_5).addE("snps").to(nc_000010_11_g_94775160G_C).next();

    var gt_demo_1 = g.addV("genotype").property("id", "gt_demo_1").property("name", "gt_demo_1").next();
    g.V(gt_demo_1).addE("haplotype").to(hap_demo_1).next();

    var gt_demo_2 = g.addV("genotype").property("id", "gt_demo_2").property("name", "gt_demo_2").next();
    g.V(gt_demo_2).addE("haplotype").to(hap_demo_2).next();

    var gt_demo_3 = g.addV("genotype").property("id", "gt_demo_3").property("name", "gt_demo_3").next();
    g.V(gt_demo_3).addE("haplotype").to(hap_demo_1).next();
    g.V(gt_demo_3).addE("haplotype").to(hap_demo_3).next();

    var gt_demo_4 = g.addV("genotype").property("id", "gt_demo_4").property("name", "gt_demo_4").next();
    g.V(gt_demo_4).addE("haplotype").to(hap_demo_4).next();
    g.V(gt_demo_4).addE("haplotype").to(hap_demo_5).next();

    var ph_demo_1 = g.addV("phenotype").property("id", "ph_demo_1").property("name", "ph_demo_1").next();
    g.V(ph_demo_1).addE("genotypes").to(gt_demo_1).next();
    g.V(ph_demo_1).addE("genotypes").to(gt_demo_2).next();
    g.V(ph_demo_1).addE("genotypes").to(gt_demo_3).next();

    var ph_demo_2 = g.addV("phenotype").property("id", "ph_demo_2").property("name", "ph_demo_2").next();
    g.V(ph_demo_2).addE("genotypes").to(gt_demo_4).next();
    g.tx().commit();
  }


  private void createSchema(JanusGraphManagement mgmt) {
    // Properties
    var id = mgmt.makePropertyKey("id").cardinality(Cardinality.SINGLE).dataType(String.class).make();
    var name = mgmt.makePropertyKey("name").cardinality(Cardinality.LIST).dataType(String.class).make();
    var accession = mgmt.makePropertyKey("accession").cardinality(Cardinality.SINGLE).dataType(String.class).make();
    var isTopLevelAssembly = mgmt.makePropertyKey("is_top_level").cardinality(Cardinality.SINGLE).dataType(Boolean.class).make();
    var isAltAssembly = mgmt.makePropertyKey("is_alt").cardinality(Cardinality.SINGLE).dataType(Boolean.class).make();
    var isPatchAssembly = mgmt.makePropertyKey("is_patch").cardinality(Cardinality.SINGLE).dataType(Boolean.class).make();
    var isChromosomeAssembly = mgmt.makePropertyKey("is_chormosome").cardinality(Cardinality.SINGLE).dataType(Boolean.class).make();

    var snpPos = mgmt.makePropertyKey("snp_position").cardinality(Cardinality.SINGLE).dataType(Integer.class).make();
    var refAllele = mgmt.makePropertyKey("ref_allele").cardinality(Cardinality.SINGLE).dataType(String.class).make();
    var altAllele = mgmt.makePropertyKey("alt_allele").cardinality(Cardinality.SINGLE).dataType(String.class).make();

    // Vertex
    // Assembly
    var assembly = mgmt.makeVertexLabel("assembly").make();
    mgmt.addProperties(assembly, id, name, accession, isTopLevelAssembly, isAltAssembly, isPatchAssembly, isChromosomeAssembly);
    
    // Sequence
    var sequence = mgmt.makeVertexLabel("sequence").make();
    mgmt.addProperties(sequence, id, name);

    // SNP
    var snp = mgmt.makeVertexLabel("snp").make();
    mgmt.addProperties(snp, id);

    // SNP definition
    var snpDefinition = mgmt.makeVertexLabel("snp_definition").make();
    mgmt.addProperties(snpDefinition, id, name, snpPos, refAllele, altAllele);

    // Haplotype
    var haplotype = mgmt.makeVertexLabel("haplotype").make();
    mgmt.addProperties(haplotype, id, name);

    // Genotype
    var genotype = mgmt.makeVertexLabel("genotype").make();
    mgmt.addProperties(genotype, id);

    // Phenotype
    var phenotype = mgmt.makeVertexLabel("phenotype").make();
    mgmt.addProperties(phenotype, id);

    // Relationships

    // Assembly --> Sequences
    var sequencesEdge = mgmt.makeEdgeLabel("sequences").multiplicity(Multiplicity.MANY2ONE).make();
    mgmt.addConnection(sequencesEdge, assembly, sequence);

    // Sequence --> Assembly
    var assemblyEdge  = mgmt.makeEdgeLabel("assembly").multiplicity(Multiplicity.MANY2ONE).make();
    mgmt.addConnection(assemblyEdge, sequence, assembly);

    // Snp --> SnpDefinition
    var snpDefinitionsEdge = mgmt.makeEdgeLabel("snp_definitions").multiplicity(Multiplicity.ONE2MANY).make();
    mgmt.addConnection(snpDefinitionsEdge, snp, snpDefinition);

    // Haplotype --> Snps
    var snpsEdge = mgmt.makeEdgeLabel("snps").multiplicity(Multiplicity.MULTI).make();
    mgmt.addConnection(snpsEdge, haplotype, snp);

    // Genotype --> Haplotypes
    var haplotypesEdge = mgmt.makeEdgeLabel("haplotypes").multiplicity(Multiplicity.MULTI).make();
    mgmt.addConnection(haplotypesEdge, genotype, haplotype);

    // Phenotype --> Genotypes
    var genotypesEdge = mgmt.makeEdgeLabel("genotypes").multiplicity(Multiplicity.MULTI).make();
    mgmt.addConnection(genotypesEdge, phenotype, genotype);

    mgmt.commit();
  }
}