package com.indimedis.knowledgebase.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Variant {
  @JsonProperty("chr") String chr;
  @JsonProperty("pos") Integer pos;
  @JsonProperty("ref") String refAllele;
  @JsonProperty("alt") String[] altAlleles;
  @JsonProperty("gt")Integer[] gt;
}