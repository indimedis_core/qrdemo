package com.indimedis.knowledgebase;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.janusgraph.core.JanusGraphFactory;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.core.JanusGraph;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigProperties {

  @Bean(destroyMethod = "close")
  public JanusGraph janusGraph() throws ConfigurationException, BackendException {
    org.apache.commons.configuration.Configuration conf = new PropertiesConfiguration("conf/jgex-cql.properties");
    JanusGraph janusGraph = JanusGraphFactory.open(conf);
    JanusGraphFactory.drop(janusGraph);
    return janusGraph = JanusGraphFactory.open(conf);
  }

  @Bean(destroyMethod = "close")
  public GraphTraversalSource g() throws ConfigurationException, BackendException {
    return janusGraph().traversal();
  }
  
}