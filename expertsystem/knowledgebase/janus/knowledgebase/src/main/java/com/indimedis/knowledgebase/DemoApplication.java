package com.indimedis.knowledgebase;

import javax.annotation.PreDestroy;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.janusgraph.core.JanusGraph;
import org.janusgraph.core.JanusGraphException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	@Autowired
	JanusGraph janusGraph;
	
	@Autowired
  GraphTraversalSource g;

	public static void main(final String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@PreDestroy
  public void onExit() {
	}
}
