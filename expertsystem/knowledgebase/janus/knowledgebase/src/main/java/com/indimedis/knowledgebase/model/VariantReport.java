package com.indimedis.knowledgebase.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class VariantReport {

	@JsonProperty("assembly") String assembly;
	@JsonProperty("date") String date;
	@JsonProperty("vars") Variant[] variants;

}