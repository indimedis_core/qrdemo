package com.indimedis.knowledgebase;

import java.util.concurrent.atomic.AtomicLong;

import com.indimedis.knowledgebase.model.Greeting;
import com.indimedis.knowledgebase.model.PGxReport;
import com.indimedis.knowledgebase.model.VariantReport;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import static org.apache.tinkerpop.gremlin.process.traversal.P.*;
import static org.apache.tinkerpop.gremlin.process.traversal.Pop.*;
import static org.apache.tinkerpop.gremlin.process.traversal.SackFunctions.*;
import static org.apache.tinkerpop.gremlin.process.traversal.Scope.*;
import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class PgxController {

	@Autowired
	GraphTraversalSource g;

	@PostMapping("/pgxreport")
	public @ResponseBody PGxReport post(@RequestBody VariantReport report) {
		log.info(report.toString());

		var snps = g.V().hasLabel("snp").has("id", "NC_000001_11_g_97079071C_A")
								.V().hasLabel("snp").has("id", "NC_000001_11_g_97450058C_T").toList();
	
		var assembly = report.getAssembly();
		var vlist = g.V().hasLabel("haplotype").where(out("snps").has("id", 
			without(
				"NC_000001_11_g_97079071C_A", 
				"NC_000001_11_g_97450058C_T"
				)
			).count().is(
				lt(1)
				)
			).toList();

	// 	if (vertex.isPresent()) {
	// 		log.info(vertex.get().toString());
	// } else {
	// 		log.warn("jupiter not found");
	// }

	// var map = g.V(vertex.get()).elementMap().toList();
	// log.info(map.toString());

	for(var v : vlist) {
		log.info(g.V(v).elementMap().toList().toString());
	}


		var pgxreport = new PGxReport();
		pgxreport.setHallo("Hallo");
		return pgxreport;
	}
}