package phenotyper

// import "strings"

// type PhenotypeMatcher struct {
// 	phenotypeIndex map[string]Result
// }

// type Result struct {
// 	GepGT     string
// 	Phenotype string
// }

// func NewPhenotypeMatcher(phenotypes []*Phenotype) *PhenotypeMatcher {
// 	matcher := PhenotypeMatcher{}
// 	matcher.buildIndex(phenotypes)
// 	return &matcher
// }

// func (matcher *PhenotypeMatcher) buildIndex(phenotypes []*Phenotype) {
// 	phenIdx := map[string]Result{}
// 	for _, pheno := range phenotypes {
// 		for _, gepgt := range pheno.GepGT {
// 			key := strings.Join(gepgt.Geps, "_")
// 			phenIdx[key] = Result{
// 				GepGT:     gepgt.URI,
// 				Phenotype: pheno.URI}
// 		}
// 	}

// 	matcher.phenotypeIndex = phenIdx
// }

// func (matcher *PhenotypeMatcher) Search(gepURI1, gepURI2 string) *Result {
// 	possibleKeys := []string{gepURI1 + "_" + gepURI2, gepURI2 + "_" + gepURI1}

// 	for _, key := range possibleKeys {
// 		if v, ok := matcher.phenotypeIndex[key]; ok {
// 			return &v
// 		}
// 	}
// 	return nil
// }
