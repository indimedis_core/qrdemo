package phenotyper

// import (
// 	"bytes"
// 	"encoding/json"
// 	"errors"
// 	"log"
// 	"net/http"
// 	"time"
// )

// type Phenotyper struct {
// 	PhenotypeMatchers map[string]*PhenotypeMatcher
// 	GepDecisionTrees  map[string]*InternalTree
// 	GuidelineIDs      []string
// }

// type Guideline struct {
// 	ID           string       `json:"id"`
// 	Phenotypes   []*Phenotype `json:"phenotypes"`
// 	GepStructure []*GepNode   `json:"gepStructure"`
// }

// type Phenotype struct {
// 	URI   string   `json:"uri"`
// 	GepGT []*GepGT `json:"gepgt"`
// }

// type GepGT struct {
// 	URI  string   `json:"uri"`
// 	Geps []string `json:"geps"`
// }

// // type GepNodeTree struct {
// // 	GepNodes []*GepNode `json:"gepStructure"`
// // }

// type GepNode struct {
// 	URI           string   `json:"uri"`
// 	ActivityScore float64  `json:"activityScore"`
// 	Source        string   `json:"source"`
// 	Children      []string `json:"children"`
// 	Parents       []string `json:"parents"`
// }

// func NewPhenotyper(guidelineStructureEndpt string, guidelineIDs []string) *PhenotypeReportBuilder {
// 	reportBuilder := PhenotypeReportBuilder{
// 		PhenotypeMatchers: map[string]*PhenotypeMatcher{},
// 		GepDecisionTrees:  map[string]*InternalTree{},
// 		GuidelineIDs:      guidelineIDs,
// 	}

// 	for _, v := range guidelineIDs {
// 		guideline := getGuideline(guidelineStructureEndpt, v)
// 		if guideline != nil {
// 			reportBuilder.PhenotypeMatchers[v] = NewPhenotypeMatcher(guideline.Phenotypes)
// 			reportBuilder.GepDecisionTrees[v] = NewInternalTree(guideline.GepStructure)
// 		}
// 	}

// 	return &reportBuilder
// }

// func (b *PhenotypeReportBuilder) CreateReport(snpReport kafkamsg.SnpReport) (*kafkamsg.PhenotypeReport, error) {
// 	patID := snpReport.ID

// 	if len(snpReport.Strands) == 2 {
// 		results := []kafkamsg.PgxTriple{}
// 		strandURI1 := snpReport.Strands[0].ID
// 		strandURI2 := snpReport.Strands[1].ID
// 		for _, gID := range b.GuidelineIDs {

// 			gepResult := b.GepDecisionTrees[gID].Analize(snpReport)
// 			if len(gepResult) == 2 {
// 				phenotypeResult := b.PhenotypeMatchers[gID].Search(gepResult[strandURI1], gepResult[strandURI2])
// 				results = append(results, kafkamsg.PgxTriple{
// 					Guideline: gID,
// 					GepGT:     phenotypeResult.GepGT,
// 					Phenotype: phenotypeResult.Phenotype})
// 			} else {
// 				return nil, errors.New("unexpected length of gep result")
// 			}
// 		}
// 		return &kafkamsg.PhenotypeReport{
// 			PatID:      patID,
// 			PgxTriples: results,
// 		}, nil
// 	}
// 	return nil, errors.New("unexpected number of strands")
// }

// func getGuideline(guidelineStructureEndpt, guidelineID string) *Guideline {
// 	log.Println("got new request: ", guidelineID)
// 	var reqData = []byte("{ \"guidelineId\": \"" + guidelineID + "\" }")
// 	req, err := http.NewRequest("POST", guidelineStructureEndpt, bytes.NewBuffer(reqData))
// 	if err != nil {
// 		log.Fatal(err, guidelineStructureEndpt, "Failed to get guideline 1")
// 	}
// 	req.Header.Set("Content-Type", "application/json")

// 	client := &http.Client{}
// 	for {
// 		resp, err := client.Do(req)
// 		if err == nil {
// 			defer resp.Body.Close()
// 			guideline := Guideline{}
// 			err = json.NewDecoder(resp.Body).Decode(&guideline)
// 			if err == nil {
// 				if resp.StatusCode == 200 {
// 					return &guideline
// 				}

// 			}
// 			log.Println(resp.StatusCode, ": ", resp.Status, " ", err)
// 		} else {
// 			log.Fatal("Error on request: ", err)
// 		}
// 		time.Sleep(3 * time.Second)
// 	}
// }
