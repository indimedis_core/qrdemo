package phenotyper

// import (
// 	"fmt"
// 	"math"
// 	"sort"

// 	"indimedis.com/phenotypereportbuilder/pkg/kafkamsg"
// )

// type InternalTree struct {
// 	Nodes    map[string]*InternalNode
// 	SnpNodes []string
// }

// type InternalNode struct {
// 	URI           string
// 	ActivityScore float64
// 	Source        string
// 	Children      []*InternalNode
// 	Parents       []*InternalNode
// 	Status        bool
// }

// func (node *InternalNode) SetStatusFalse() {
// 	node.Status = false
// 	node.propagateStatus()
// }

// func (node *InternalNode) propagateStatus() {
// 	for _, parent := range node.Parents {
// 		parent.SetStatusFalse()
// 	}
// }

// func (tree *InternalTree) Analize(snpReport kafkamsg.SnpReport) map[string]string {
// 	result := map[string]string{}

// 	for _, strand := range snpReport.Strands {
// 		tree.reset()
// 		matchingNodes := []*InternalNode{}

// 		for _, snpNode := range tree.SnpNodes {
// 			if !stringInSlice(snpNode, strand.Snps) {
// 				//				println(snpNode)
// 				tree.Nodes[snpNode].SetStatusFalse()
// 			}
// 		}

// 		for _, node := range tree.Nodes {
// 			if node.Status && !stringInSlice(node.URI, tree.SnpNodes) {
// 				fmt.Println(strand.ID, " - ", node.URI)
// 				matchingNodes = append(matchingNodes, node)
// 			}
// 		}

// 		if len(matchingNodes) == 0 {
// 			matchingNodes = append(matchingNodes, tree.Nodes["<http://curatip.com/clinical/curatology#GEP_0_0>"])
// 		}
// 		matchingNodes = tree.removeChildsOfParents(matchingNodes)
// 		sort.Slice(matchingNodes, func(i, j int) bool {
// 			if matchingNodes[i] == nil {
// 				return false
// 			}

// 			if matchingNodes[j] == nil {
// 				return true
// 			}

// 			score1 := math.Abs(1.0 - matchingNodes[i].ActivityScore)
// 			score2 := math.Abs(matchingNodes[j].ActivityScore)
// 			if score1 > score2 {
// 				return true
// 			}
// 			if score1 < score2 {
// 				return false
// 			}
// 			return score1 > score2
// 		})
// 		result[strand.ID] = matchingNodes[0].URI
// 	}

// 	return result

// }

// func (tree *InternalTree) removeChildsOfParents(nodes []*InternalNode) []*InternalNode {
// 	childNodes := collectChildNodeURIs(nodes)
// 	nodesToRemove := []int{}
// 	for _, uri := range childNodes {
// 		for i, nodes := range nodes {
// 			if uri == nodes.URI {
// 				nodesToRemove = append(nodesToRemove, i)
// 			}
// 		}
// 	}
// 	for _, i := range nodesToRemove {
// 		nodes[i] = nil
// 	}

// 	i := 0
// 	for i < len(nodes) {
// 		if nodes[i] == nil {
// 			nodes[i] = nodes[len(nodes)-1]
// 			nodes[len(nodes)-1] = nil
// 			nodes = nodes[:len(nodes)-1]
// 		}
// 		i++
// 	}

// 	return nodes
// }

// func collectChildNodeURIs(nodes []*InternalNode) []string {
// 	childNodes := []string{}
// 	for _, node := range nodes {
// 		for _, child := range node.Children {
// 			childNodes = append(childNodes, child.URI)
// 		}
// 	}
// 	return childNodes
// }

// func (tree *InternalTree) reset() {
// 	// reset tree
// 	for _, node := range tree.Nodes {
// 		node.Status = true
// 	}
// }

// func NewInternalTree(treeNodes []*GepNode) *InternalTree {
// 	newTree := InternalTree{
// 		Nodes: map[string]*InternalNode{}}

// 	for _, gNode := range treeNodes {
// 		node := InternalNode{
// 			URI:           gNode.URI,
// 			ActivityScore: gNode.ActivityScore,
// 			Source:        gNode.Source,
// 			Children:      []*InternalNode{},
// 			Parents:       []*InternalNode{},
// 			Status:        true}
// 		newTree.Nodes[gNode.URI] = &node
// 	}

// 	for _, gNode := range treeNodes {

// 		if len(gNode.Children) != 0 {
// 			for _, child := range gNode.Children {
// 				newTree.Nodes[gNode.URI].Children = append(newTree.Nodes[gNode.URI].Children, newTree.Nodes[child])
// 			}
// 		} else {
// 			newTree.SnpNodes = append(newTree.SnpNodes, gNode.URI)
// 		}
// 		for _, parent := range gNode.Parents {
// 			newTree.Nodes[gNode.URI].Parents = append(newTree.Nodes[gNode.URI].Parents, newTree.Nodes[parent])
// 		}
// 	}

// 	return &newTree
// }

// func stringInSlice(a string, list []string) bool {
// 	for _, b := range list {
// 		if b == a {
// 			return true
// 		}
// 	}
// 	return false
// }
