package cdss

type CDSSConfig struct {
	Substances []TestSubstance `yaml:"substances"`
	// Sources    []Source        `yaml:"sources"`
}

type TestSubstance struct {
	Name       string          `yaml:"name"`
	Guidelines []TestGuideline `yaml:"guidelines"`
}

type TestSource struct {
	URL  string `yaml:"url"`
	Icon string `yaml:"icon"`
}

type TestGuideline struct {
	Name            string       `yaml:"name"`
	Source          TestSource   `yaml:"source"`
	PreTestAdvices  []TestAdvice `yaml:"pretestadvices"`
	PostTestAdvices []TestAdvice `yaml:"posttestadvices"`
}

type TestAdvice struct {
	Name         string            `yaml:"name"`
	Phenotypes   []TestPhenotype   `yaml:"phenotypes"`
	Conditions   TestConditions    `yaml:"conditions"`
	Descriptions []TestDescription `yaml:"descriptions"`
}

type TestDescription struct {
	Language string `yaml:"lang"`
	Title    string `yaml:"title"`
	Text     string `yaml:"text"`
}

type TestConditions struct {
	MinAge int `yaml:"minAge"`
	MaxAge int `yaml:"maxAge"`
}

type TestPhenotype struct {
	Name      string         `yaml:"name"`
	Genotypes []TestGenotype `yaml:"genotypes"`
}

type TestGenotype struct {
	Snps []TestSnp `yaml:"snps"`
}

type TestSnp struct {
	Hgvs string `yaml:"hgvs"`
	Chr  string `yaml:"chr"`
	Pos  int    `yaml:"pos"`
	Ref  string `yaml:"ref"`
	Alt  string `yaml:"alt"`
}

// func TestReadFacts(t *testing.T) {
// 	data, err := ioutil.ReadFile("../facts_data.yaml")
// 	if err != nil {
// 		logrus.Error(err)
// 	}
// 	var facts CDSSConfig
// 	err = yaml.Unmarshal(data, &facts)
// 	if err != nil {
// 		logrus.Error(err)
// 	}
// 	logrus.Infoln(facts)
// 	json, err := json.MarshalIndent(facts, "", "  ")
// 	if err != nil {
// 		logrus.Error(err)
// 	}
// 	logrus.Infof("%s", json)
// }
