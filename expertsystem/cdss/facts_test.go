package cdss

import (
	"testing"

	"github.com/sirupsen/logrus"
)

var testSnp1 = Snp{
	Hgvs: "testSnp1",
	Chr:  "1",
	Pos:  12345,
	Ref:  "A",
	Alt:  "T",
}

var testSnp2 = Snp{
	Hgvs: "testSnp2",
	Chr:  "2",
	Pos:  12345,
	Ref:  "A",
	Alt:  "T",
}

var testGenotype1 = Genotype{
	Snps: []Snp{
		testSnp1,
		testSnp1,
	},
}

var testGenotype2 = Genotype{
	Snps: []Snp{
		testSnp1,
		testSnp2,
	},
}

var testGenotype3 = Genotype{
	Snps: []Snp{
		testSnp1,
	},
}

var testPhenotype1 = Phenotype{
	Name: "testPhenotype1",
	Genotypes: []Genotype{
		testGenotype1,
	},
}

var testPhenotype2 = Phenotype{
	Name: "testPhenotype2",
	Genotypes: []Genotype{
		testGenotype2,
	},
}

var testPhenotype3 = Phenotype{
	Name: "testPhenotype3",
	Genotypes: []Genotype{
		testGenotype1,
		testGenotype2,
	},
}

var testConditions1 = Conditions{
	MinAge: PInt(18),
}

var testConditions2 = Conditions{
	MinAge: PInt(18),
	MaxAge: PInt(0),
}

var testDescription1 = Description{
	Language: "en",
	Title:    "testTitle",
	Text:     "testText",
}

var testDescription2 = Description{
	Language: "de",
	Title:    "testTitel",
	Text:     "testText",
}

var testAdvice1 = Advice{
	Name: "testAdvice1",
	Phenotypes: []Phenotype{
		testPhenotype1,
		testPhenotype2,
	},
	Conditions: testConditions1,
	Descriptions: []Description{
		testDescription1,
	},
}

var testAdvice2 = Advice{
	Name: "testAdvice2",
	Phenotypes: []Phenotype{
		testPhenotype1,
	},
	Conditions: testConditions1,
	Descriptions: []Description{
		testDescription1,
	},
}

var testAdvice3 = Advice{
	Name: "testAdvice1",
	Phenotypes: []Phenotype{
		testPhenotype1,
		testPhenotype3,
	},
	Conditions: testConditions1,
	Descriptions: []Description{
		testDescription1,
	},
}

var testSource1 = Source{
	URL:  "testSource1",
	Icon: "testIcon1",
}

var testSource2 = Source{
	URL:  "testSource2",
	Icon: "testIcon2",
}

var testGuideline1 = Guideline{
	Name:   "testGuideline1",
	Source: testSource1,
	PreTestAdvices: []Advice{
		testAdvice1,
		testAdvice2,
	},
	PostTestAdvices: []Advice{
		testAdvice3,
	},
}

var testGuideline2 = Guideline{
	Name:   "testGuideline2",
	Source: testSource1,
	PreTestAdvices: []Advice{
		testAdvice1,
		testAdvice2,
	},
	PostTestAdvices: []Advice{
		testAdvice3,
	},
}

var testGuideline3 = Guideline{
	Name:   "testGuideline1",
	Source: testSource1,
	PreTestAdvices: []Advice{
		testAdvice1,
		testAdvice2,
	},
	PostTestAdvices: []Advice{
		testAdvice1,
	},
}

var testSubstance1 = Substance{
	Name: "testSubstance1",
	Guidelines: []Guideline{
		testGuideline1,
		testGuideline2,
	},
}

var testSubstance2 = Substance{
	Name: "testSubstance2",
	Guidelines: []Guideline{
		testGuideline1,
		testGuideline2,
	},
}

var testSubstance3 = Substance{
	Name: "testSubstance1",
	Guidelines: []Guideline{
		testGuideline1,
		testGuideline2,
		testGuideline3,
	},
}

var testFacts1 = Facts{
	Substances: []Substance{
		testSubstance1,
	},
	Snps: []Snp{
		testSnp1,
	},
	Genotypes: []Genotype{
		testGenotype1,
	},
	Phenotypes: []Phenotype{
		testPhenotype1,
	},
}

var testFacts2 = Facts{
	Substances: []Substance{
		testSubstance1,
	},
	Snps: []Snp{
		testSnp1,
	},
	Genotypes: []Genotype{
		testGenotype1,
	},
}

var testFacts3 = Facts{
	Substances: []Substance{
		testSubstance1,
	},
	Snps: []Snp{
		testSnp1,
		testSnp2,
	},
	Genotypes: []Genotype{
		testGenotype1,
	},
	Phenotypes: []Phenotype{
		testPhenotype1,
	},
}

var yamlSource1 = Source{
	URL:  "https://cpicpgx.org/",
	Icon: "https://cpicpgx.org/wp-content/uploads/2016/01/cpic-200.png?v=2",
}

var c703_c_t = Snp{
	Hgvs: "c.703C>T (*8)",
	Chr:  "1",
	Pos:  97691776,
	Ref:  "G",
	Alt:  "A",
}
var c61_c_t = Snp{
	Hgvs: "c.61C>T",
	Chr:  "1",
	Pos:  97883353,
	Ref:  "G",
	Alt:  "A",
}
var c557_a_g = Snp{
	Hgvs: "c.557A>G",
	Chr:  "1",
	Pos:  97699474,
	Ref:  "T",
	Alt:  "C",
}

var c703_c_t_c703_c_t = Genotype{
	Snps: []Snp{
		c703_c_t,
		c703_c_t,
	},
}
var c703_c_t_c61_c_t = Genotype{
	Snps: []Snp{
		c703_c_t,
		c61_c_t,
	},
}
var c703_c_t_c557_a_g = Genotype{
	Snps: []Snp{
		c703_c_t,
		c557_a_g,
	},
}
var c557_a_g_c557_a_g = Genotype{
	Snps: []Snp{
		c557_a_g,
		c557_a_g,
	},
}

var CPIC_5FU_Poor_Metabolizer_0 = Phenotype{
	Name: "5-FU DPYD Poor Metabolizer (Activity score 0)",
	Genotypes: []Genotype{
		c703_c_t_c703_c_t,
		c703_c_t_c61_c_t,
	},
}
var CPIC_5FU_Poor_Metabolizer_0_5 = Phenotype{
	Name: "5-FU DPYD Poor Metabolizer (Activity score 0.5)",
	Genotypes: []Genotype{
		c703_c_t_c557_a_g,
	},
}
var CPIC_5FU_Intermediate_Metabolizer_1 = Phenotype{
	Name: "5-FU DPYD Intermediate Metabolizer (Activity score 1)",
	Genotypes: []Genotype{
		c557_a_g_c557_a_g,
	},
}

var yamlGuideline1 = Guideline{
	Name:   "CPIC 5-FU",
	Source: yamlSource1,
	PreTestAdvices: []Advice{
		{
			Name: "Pretest Advice",
			Descriptions: []Description{
				{
					Language: "en",
					Title:    "No DPYD result on file and a fluoropyrimidine ordered",
					Text:     "DPYD genotype may be useful for 5-fluorouracil dosing and use.  A DPYD genotype does not appear to have been ordered for this patient. Use of an alternative dose or agent may be recommended. Please consult a clinical pharmacistb for more information.",
				},
			},
		},
	},
	PostTestAdvices: []Advice{
		{
			Name: "DPYD Poor Metabolizer (Activity score 0)",
			Phenotypes: []Phenotype{
				CPIC_5FU_Poor_Metabolizer_0,
			},
			Descriptions: []Description{
				{
					Language: "en",
					Title:    "DPYD Poor Metabolizer (Activity score 0)  and fluoropyrimidine ordered",
					Text:     "Based on the genotype result, this patient is predicted to be a DPYD Poor Metabolizer (Activity score 0) and has an increased risk of severe toxicity when treated with 5-fluorouracil.  Avoid use of 5-fluoruracil or 5-fluoruracil prodrug-based regimens in this patient. Please consult a clinical pharmacistb for more information.",
				},
			},
		},
		{
			Name: "DPYD Poor Metabolizer (Activity score 0.5)",
			Phenotypes: []Phenotype{
				CPIC_5FU_Poor_Metabolizer_0_5,
			},
			Descriptions: []Description{
				{
					Language: "en",
					Title:    "DPYD Poor Metabolizer (Activity score 0.5)  and fluoropyrimidine  ordered",
					Text:     "Based on the genotype result, this patient is predicted to be a DPYD Poor Metabolizer (Activity score 0.5) and has an increased risk of severe toxicity when treated with 5-fluorouracil.  Avoid use of 5-fluoruracil or 5-fluoruracil prodrug-based regimens in this patient. In the event, based on clinical advice, alternative agents are not considered a suitable therapeutic option, 5-fluoruracil should be administered at a strongly reduced dosed with early therapeutic drug monitoring in order to immediately discontinue therapy if the drug level is too high.  Please consult a clinical pharmacistb for more information.",
				},
			},
		},
		{
			Name: "DPYD Poor Metabolizer (Activity score 1)",
			Phenotypes: []Phenotype{
				CPIC_5FU_Intermediate_Metabolizer_1,
			},
			Descriptions: []Description{
				{
					Language: "en",
					Title:    "DPYD Intermediate Metabolizer (Activity score 1)  and fluoropyrimidine ordered",
					Text:     "Based on the genotype result, this patient is predicted to be a DPYD Intermediate Metabolizer (Activity score 1) and has an increased risk of severe toxicity when treated with 5-fluorouracil.  Reduce starting dose by 50% followed by titration of dose based on toxicityc or therapeutic drug monitoring (if available). Please consult a clinical pharmacistb for more information.",
				},
			},
		},
	},
}

var yamlSubstance = Substance{
	Name: "5-FU",
	Guidelines: []Guideline{
		yamlGuideline1,
	},
}

var yamlFacts = Facts{
	Substances: []Substance{
		yamlSubstance,
	},
	Snps: []Snp{
		c703_c_t,
		c61_c_t,
		c557_a_g,
	},
	Genotypes: []Genotype{
		c703_c_t_c703_c_t,
		c703_c_t_c61_c_t,
		c703_c_t_c557_a_g,
		c557_a_g_c557_a_g,
	},
	Phenotypes: []Phenotype{
		CPIC_5FU_Poor_Metabolizer_0,
		CPIC_5FU_Poor_Metabolizer_0_5,
		CPIC_5FU_Intermediate_Metabolizer_1,
	},
}

func TestFactsEquals(t *testing.T) {
	if !testFacts1.Equals(testFacts1) {
		logrus.Error("Equal Facts are unequal: testFacts1 != testFacts1")
	}
	if testFacts1.Equals(testFacts2) {
		logrus.Error("Unequal Facts are equal: testFacts1 == testFacts2")
	}
	if testFacts1.Equals(testFacts3) {
		logrus.Error("Unequal Facts are equal: testFacts1 == testFacts3")
	}
}

func TestSubstanceEquals(t *testing.T) {
	if testSubstance1.Equals(testSubstance1) != true {
		logrus.Error("Equal Substance are unequal: testSubstance1 != testSubstance1")
	}
	if testSubstance1.Equals(testSubstance2) {
		logrus.Error("Unequal Substance are equal: testSubstance1 == testSubstance2")
	}
	if testSubstance1.Equals(testSubstance3) {
		logrus.Error("Unequal Substance are equal: testSubstance1 == testSubstance3")
	}
}

func TestGuidelineEquals(t *testing.T) {
	if testGuideline1.Equals(testGuideline1) != true {
		logrus.Error("Equal Guideline are unequal: testGuideline1 != testGuideline1")
	}
	if testGuideline1.Equals(testGuideline2) {
		logrus.Error("Unequal Guideline are equal: testGuideline1 == testGuideline2")
	}
	if testGuideline1.Equals(testGuideline3) {
		logrus.Error("Unequal Guideline are equal: testGuideline1 == testGuideline3")
	}
}

func TestSourceEquals(t *testing.T) {
	if testSource1.Equals(testSource1) != true {
		logrus.Error("Equal Source are unequal: testSource1 != testSource1")
	}
	if testSource1.Equals(testSource2) {
		logrus.Error("Unequal Source are equal: testSource1 == testSource2")
	}
}

func TestAdviceEquals(t *testing.T) {
	if testAdvice1.Equals(testAdvice1) != true {
		logrus.Error("Equal Advice are unequal: testAdvice1 != testAdvice1")
	}
	if testAdvice1.Equals(testAdvice2) {
		logrus.Error("Unequal Advice are equal: testAdvice1 == testAdvice2")
	}
	if testAdvice1.Equals(testAdvice3) {
		logrus.Error("Unequal Advice are equal: testAdvice1 == testAdvice3")
	}
}

func TestDescriptionEquals(t *testing.T) {
	if testDescription1.Equals(testDescription1) != true {
		logrus.Error("Equal Description are unequal: testDescription1 != testDescription1")
	}
	if testDescription1.Equals(testDescription2) {
		logrus.Error("Unequal Description are equal: testDescription1 == testDescription2")
	}
}

func TestConditionsEquals(t *testing.T) {
	if testConditions1.Equals(testConditions1) != true {
		logrus.Error("Equal Conditions are unequal: testConditions1 != testConditions1")
	}
	if testConditions1.Equals(testConditions2) {
		logrus.Error("Unequal Conditions are equal: testConditions1 == testConditions2")
	}
}

func TestPhenotypeEquals(t *testing.T) {
	if testPhenotype1.Equals(testPhenotype1) != true {
		logrus.Error("Equal Phenotype are unequal: testPhenotype1 != testPhenotype1")
	}
	if testPhenotype1.Equals(testPhenotype2) {
		logrus.Error("Unequal Phenotype are equal: testPhenotype1 == testPhenotype2")
	}
	if testPhenotype1.Equals(testPhenotype3) {
		logrus.Error("Phenotype with unequal Genotype length are equal: testPhenotype1 == testPhenotype3")
	}
}

func TestGenotypeEquals(t *testing.T) {
	if testGenotype1.Equals(testGenotype1) != true {
		logrus.Error("Equal Genotype are unequal: testGenotype1 != testGenotype1")
	}
	if testGenotype1.Equals(testGenotype2) {
		logrus.Error("Unequal Genotype are equal: testGenotype1 == testGenotype2")
	}
	if testGenotype1.Equals(testGenotype3) {
		logrus.Error("Genotypes with unequal Snp length are equal: testGenotype1 == testGenotype3")
	}
}

func TestSnpEquals(t *testing.T) {
	if testSnp1.Equals(testSnp1) != true {
		logrus.Error("Equal Snps are unequal: testSnp1 != testSnp1")
	}
	if testSnp1.Equals(testSnp2) {
		logrus.Error("Unequal Snps are equal: testSnp1 == testSnp2")
	}
}

func TestReadFacts(t *testing.T) {
	data, err := readFacts("testdata/facts_data.yaml")
	if err != nil {
		logrus.Error(err)
		t.FailNow()
	}

	if !yamlFacts.Equals(*data) {
		logrus.Error("yamlFacts != data read from testdata/facts_data.yaml")
		t.FailNow()
	}
}
