package cdss

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

func PInt(i int) *int {
	return &i
}

type Facts struct {
	Substances []Substance `json:"substances"`
	Snps       []Snp       `json:"snps"`
	Genotypes  []Genotype  `json:"genotypes"`
	Phenotypes []Phenotype `json:"phenotypes"`
	Guidelines []Guideline `json:"guidelines"`
}

func (f Facts) Equals(f2 Facts) bool {
	if len(f.Substances) != len(f2.Substances) {
		return false
	}
	if len(f.Snps) != len(f2.Snps) {
		return false
	}
	if len(f.Genotypes) != len(f2.Genotypes) {
		return false
	}
	if len(f.Phenotypes) != len(f2.Phenotypes) {
		return false
	}
	if len(f.Guidelines) != len(f2.Guidelines) {
		return false
	}
	for i := 0; i < len(f.Substances); i++ {
		if !f.Substances[i].Equals(f2.Substances[i]) {
			return false
		}
	}
	for i := 0; i < len(f.Snps); i++ {
		if !f.Snps[i].Equals(f2.Snps[i]) {
			return false
		}
	}
	for i := 0; i < len(f.Genotypes); i++ {
		if !f.Genotypes[i].Equals(f2.Genotypes[i]) {
			return false
		}
	}
	for i := 0; i < len(f.Phenotypes); i++ {
		if !f.Phenotypes[i].Equals(f2.Phenotypes[i]) {
			return false
		}
	}
	for i := 0; i < len(f.Guidelines); i++ {
		if !f.Guidelines[i].Equals(f2.Guidelines[i]) {
			return false
		}
	}
	return true
}

type Substance struct {
	ID   string `yaml:"id"`
	Name string `yaml:"name"`
}

func (s Substance) Equals(s2 Substance) bool {
	if s.ID != s2.ID {
		return false
	}
	if s.Name != s2.Name {
		return false
	}
	return true
}

type Source struct {
	URL  string `yaml:"url"`
	Icon string `yaml:"icon"`
}

func (s Source) Equals(s2 Source) bool {
	if s.URL == s2.URL &&
		s.Icon == s2.Icon {
		return true
	}
	return false
}

type Guideline struct {
	Name            string      `yaml:"name"`
	Source          Source      `yaml:"source"`
	Substances      []Substance `yaml:"substances"`
	PreTestAdvices  []Advice    `yaml:"pretestadvices"`
	PostTestAdvices []Advice    `yaml:"posttestadvices"`
}

func (g Guideline) Equals(g2 Guideline) bool {
	if g.Name != g2.Name {
		return false
	}
	if !g.Source.Equals(g2.Source) {
		return false
	}
	if len(g.Substances) != len(g2.Substances) {
		return false
	}
	if len(g.PreTestAdvices) != len(g2.PreTestAdvices) {
		return false
	}
	if len(g.PostTestAdvices) != len(g2.PostTestAdvices) {
		return false
	}
	for i := 0; i < len(g.Substances); i++ {
		if !g.Substances[i].Equals(g2.Substances[i]) {
			return false
		}
	}
	for i := 0; i < len(g.PreTestAdvices); i++ {
		if !g.PreTestAdvices[i].Equals(g2.PreTestAdvices[i]) {
			return false
		}
	}
	for i := 0; i < len(g.PostTestAdvices); i++ {
		if !g.PostTestAdvices[i].Equals(g2.PostTestAdvices[i]) {
			return false
		}
	}
	return true
}

type Advice struct {
	Name         string        `yaml:"name"`
	Phenotypes   []Phenotype   `yaml:"phenotypes"`
	Conditions   Conditions    `yaml:"conditions"`
	Descriptions []Description `yaml:"descriptions"`
}

func (a Advice) Equals(a2 Advice) bool {
	if a.Name != a2.Name {
		return false
	}
	if len(a.Phenotypes) != len(a2.Phenotypes) {
		return false
	}
	if len(a.Descriptions) != len(a2.Descriptions) {
		return false
	}

	if !a.Conditions.Equals(a2.Conditions) {
		return false
	}

	for i := 0; i < len(a.Phenotypes); i++ {
		if !a.Phenotypes[i].Equals(a2.Phenotypes[i]) {
			return false
		}
	}
	for i := 0; i < len(a.Descriptions); i++ {
		if !a.Descriptions[i].Equals(a2.Descriptions[i]) {
			return false
		}
	}
	return true
}

type Description struct {
	Language string `yaml:"lang"`
	Title    string `yaml:"title"`
	Text     string `yaml:"text"`
}

func (d Description) Equals(d2 Description) bool {
	if d.Language == d2.Language &&
		d.Title == d2.Title &&
		d.Text == d2.Text {
		return true
	}
	return false
}

type Conditions struct {
	MinAge *int `yaml:"minAge"`
	MaxAge *int `yaml:"maxAge"`
}

func (c Conditions) Equals(c2 Conditions) bool {
	if !(c.MaxAge == nil && c2.MaxAge == nil) {
		if c.MaxAge == nil || c2.MaxAge == nil {
			// XOR nil
			return false
		} else if *c.MaxAge != *c2.MaxAge {
			return false
		}
	}

	if !(c.MinAge == nil && c2.MinAge == nil) {
		if c.MinAge == nil || c2.MinAge == nil {
			// XOR nil
			return false
		} else if *c.MinAge != *c2.MinAge {
			return false
		}
	}

	return true
}

type Phenotype struct {
	ID        string     `yaml:"id"`
	Name      string     `yaml:"name"`
	Genotypes []Genotype `yaml:"genotypes"`
}

func (p Phenotype) Equals(p2 Phenotype) bool {
	if p.ID != p2.ID {
		return false
	}
	if p.Name != p2.Name {
		return false
	}
	if len(p.Genotypes) != len(p2.Genotypes) {
		return false
	}
	for i := 0; i < len(p.Genotypes); i++ {
		if !p.Genotypes[i].Equals(p2.Genotypes[i]) {
			return false
		}
	}
	return true
}

type Genotype struct {
	ID   string    `yaml:"id"`
	Hap1 Haplotype `yaml:"hap1"`
	Hap2 Haplotype `yaml:"hap2"`
}

func (g Genotype) Equals(g2 Genotype) bool {
	if g.ID != g2.ID {
		return false
	}
	if !g.Hap1.Equals(g2.Hap1) {
		return false
	}
	if !g.Hap2.Equals(g2.Hap2) {
		return false
	}
	return true
}

type Haplotype struct {
	ID       string   `yaml:"id"`
	Children []string `yaml:"children"`
	Parents  []string `yaml:"parents"`
}

func (p Phenotype) Equals(p2 Phenotype) bool {
	if p.ID != p2.ID {
		return false
	}
	if p.Name != p2.Name {
		return false
	}
	if len(p.Genotypes) != len(p2.Genotypes) {
		return false
	}
	for i := 0; i < len(p.Genotypes); i++ {
		if !p.Genotypes[i].Equals(p2.Genotypes[i]) {
			return false
		}
	}
	return true
}

type Snp struct {
	ID   string `yaml:"id"`
	Hgvs string `yaml:"hgvs"`
	Chr  string `yaml:"chr"`
	Pos  int    `yaml:"pos"`
	Ref  string `yaml:"ref"`
	Alt  string `yaml:"alt"`
}

func (s Snp) Equals(s2 Snp) bool {
	if s.ID == s2.ID &&
		s.Alt == s2.Alt &&
		s.Chr == s2.Chr &&
		s.Hgvs == s2.Hgvs &&
		s.Pos == s2.Pos &&
		s.Ref == s2.Ref {
		return true
	}
	return false
}

func readFacts(path string) (*Facts, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	facts := Facts{}
	err = yaml.Unmarshal(data, &facts)
	if err != nil {
		return nil, err
	}
	return &facts, nil
}
