package cdss

import (
	"fmt"
)

type CDSSystem struct {
	*Facts
	haplotypeTree HaplotypeTree
}

type VariantReport interface {
	Ref() string
	Date() string
	Vars() []Variant
}

type Variant interface {
	Chr() string
	Pos() int
	Ref() string
	Alt() []string
	Gt() []int
}

type PGxReport struct {
	ReportItems []ReportItem
}

type ReportItem struct {
	Genotype   string
	Guidelines string
}

type Sequence map[string]bool

func NewCDSSystem(factsPath string) (*CDSSystem, error) {
	facts, err := readFacts(factsPath)
	if err != nil {
		return nil, err
	}
	// vars = prepareFacts(facts)
	return &CDSSystem{
		Facts: facts,
	}, nil
}

func (c *CDSSystem) GeneratePgxReport(report VariantReport) (*PGxReport, error) {
	pgxreport := PGxReport{}
	seqs, err := separateChromSeqs(report.Vars())
	if err != nil {
		return nil, err
	}

	generateReport(seqs, &pgxreport, c.Facts)

	if err != nil {
		return nil, err
	}

	return &pgxreport, nil
}

func generateReport(seqs []Sequence, report *PGxReport, facts *Facts) {
	for _, gline := range facts.Guidelines {
		gPhenotypes := guidelinePhenotypes(gline)
		gGenotypes := getGenotypes(gPhenotypes)
	}
}

func guidelineFacts(g Guideline) []Phenotype {
	phenos := []Phenotype{}
	genos := []Genotype{}
	haps := []Haplotype{}
	for _, postAlert := range g.PostTestAdvices {
		for _, p := range postAlert.Phenotypes {
			phenos = append(phenos, p)
			for _, g := range p.Genotypes {
				genos := append(genos, g)
			}
		}
	}
	return result
}

func separateChromSeqs(vars []Variant) ([]Sequence, error) {
	seqs := []Sequence{}
	for _, v := range vars {
		seqs = adjustSeqs(seqs, len(v.Gt()))
		for i := 0; i < len(v.Gt()); i++ {
			if gt := v.Gt()[i]; gt != 0 {
				key := fmt.Sprintf("%s_%v_%s_%s", v.Chr(), v.Pos(), v.Ref(), v.Alt()[gt-1])
				seqs[i][key] = true
			}
		}
	}
	return seqs, nil
}

func findGenotypes(seqs []Sequence, facts *Facts) ([]Genotype, error) {
	result := []Genotype{}
	if len(seqs) != 2 {
		return nil, fmt.Errorf("Only diploid genotypes are supported -> have %v sequences", len(seqs))
	}
	for _, g := range facts.Genotypes {
		if len(g.Snps) != 2 {
			return nil, fmt.Errorf("Only diploid genotypes are supported -> have genotype with %v haplotypes", len(g.Snps))
		}

		snp1 := g.Snps[0]
		snp2 := g.Snps[1]
		snpKey1 := fmt.Sprintf("%s_%v_%s_%s", snp1.Chr, snp1.Pos, snp1.Ref, snp1.Alt)
		snpKey2 := fmt.Sprintf("%s_%v_%s_%s", snp2.Chr, snp2.Pos, snp2.Ref, snp2.Alt)

		if _, ok := seqs[0][snpKey1]; ok {
			if _, ok := seqs[1][snpKey2]; ok {
				result = append(result, g)
			}
		} else if _, ok := seqs[0][snpKey2]; ok {
			if _, ok := seqs[1][snpKey1]; ok {
				result = append(result, g)
			}
		}
	}
	return result, nil
}

func findPhenotypes(gts []Genotype, facts *Facts) []ReportItem {
	result := []ReportItem{}
	for _, p := range facts.Phenotypes {
		for _, pGTs := range p.Genotypes {
			for _, g := range gts {
				if g.ID == pGTs.ID {
					reportItem := ReportItem{
						Genotype:   g.ID,
						Guidelines: p.Name,
					}
					result = append(result, reportItem)
				}
			}
		}
	}
	return result
}

func adjustSeqs(seqs []Sequence, newLen int) []Sequence {
	for len(seqs) < newLen {
		seqs = append(seqs, Sequence{})
	}
	return seqs
}
