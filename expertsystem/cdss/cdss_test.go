package cdss

import (
	"testing"

	"github.com/sirupsen/logrus"
)

func TestNewCDSSystem(t *testing.T) {
	_, err := NewCDSSystem("testdata/facts_data.yaml")
	if err != nil {
		logrus.Error(err)
		t.FailNow()
	}
}

type TestVariantReport struct {
	ref  string
	date string
	vars []Variant
}

func (vr TestVariantReport) Ref() string {
	return vr.ref
}

func (vr TestVariantReport) Date() string {
	return vr.date
}

func (vr TestVariantReport) Vars() []Variant {
	return vr.vars
}

type TestVariant struct {
	chr string
	pos int
	ref string
	alt []string
	gt  []int
}

func (v TestVariant) Chr() string {
	return v.chr
}
func (v TestVariant) Pos() int {
	return v.pos
}
func (v TestVariant) Ref() string {
	return v.ref
}
func (v TestVariant) Alt() []string {
	return v.alt
}
func (v TestVariant) Gt() []int {
	return v.gt
}

var testPatients = map[string]TestVariantReport{
	"5-FU Poor Metabolizer AS 0": {
		ref:  "GRCh38",
		date: "101020",
		vars: []Variant{
			TestVariant{"1", 10616, "CCGCCGTTGCAAAGGCGCGCCG", []string{"C"}, []int{1, 0}},
			TestVariant{"1", 31884180, "T", []string{"C"}, []int{0, 1}},
			TestVariant{"1", 97691776, "G", []string{"A"}, []int{1, 1}},
		},
	},
	"5-FU Poor Metabolizer AS 0.5": {
		ref:  "GRCh38",
		date: "101020",
		vars: []Variant{
			TestVariant{"1", 10616, "CCGCCGTTGCAAAGGCGCGCCG", []string{"C"}, []int{1, 0}},
			TestVariant{"1", 31884180, "T", []string{"C"}, []int{0, 1}},
			TestVariant{"1", 97691776, "G", []string{"A"}, []int{1, 0}},
			TestVariant{"1", 97699474, "T", []string{"C"}, []int{0, 1}},
		},
	},
}

func TestGeneratePgxReport(t *testing.T) {
	cdss, err := NewCDSSystem("testdata/facts_data.yaml")
	if err != nil {
		logrus.Error(err)
		t.FailNow()
	}
	for _, pat := range testPatients {
		pgxReport, err := cdss.GeneratePgxReport(pat)
		if err != nil {
			logrus.Error(err)
			t.FailNow()
		}
		logrus.Info(pgxReport)
	}
}
