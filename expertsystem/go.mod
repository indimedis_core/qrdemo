module indimedis.com/qrdemo/expertsystem

go 1.13

require (
	github.com/dgraph-io/dgo/v2 v2.2.0
	github.com/fsnotify/fsnotify v1.4.9
	github.com/gin-gonic/gin v1.6.3
	github.com/go-ini/ini v1.56.0 // indirect
	github.com/google/uuid v1.1.1 // indirect
	github.com/minio/minio-go v6.0.14+incompatible
	github.com/northwesternmutual/grammes v1.2.0
	github.com/rs/xid v1.2.1
	github.com/segmentio/kafka-go v0.3.6
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.7.0
	go.uber.org/zap v1.15.0
	gopkg.in/yaml.v2 v2.2.8
)
