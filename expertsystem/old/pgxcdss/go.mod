module indimedis.com/pgxcdss/v2

go 1.13

require github.com/dgraph-io/dgo/v2 v2.1.0
require indimedis.com/fhir/app/genegraph latest
