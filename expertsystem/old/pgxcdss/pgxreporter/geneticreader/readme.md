# Event consumer

```graphviz
digraph finite_state_machine {
    rankdir=RL;

    node [shape = rectangle];
    "Event Consumer" -> "Genetic Reader" [ label = "1. triggers" ]
    "Genetic Reader" -> EPD [ label = "2. request data"]
    EPD -> "Genetic Reader" [ label = "3. response" ];
}
```
