package haplotyper

import "indimedis.com/pgxcdss/pgxreporter/snper"

type Result struct {
	ConfirmedHaplotypes []Haplotype
	PossibleHaplotypes  []Haplotype
	ExcludedHaplotypes  []Haplotype
}

type Haplotype struct {
	Status int // -1 = Excluded, 0 = Unknown, 1 = Confirmed
	SNPs   []snper.SNP
}
