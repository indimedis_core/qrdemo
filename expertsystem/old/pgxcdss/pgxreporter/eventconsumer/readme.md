# Event consumer

```graphviz
digraph finite_state_machine {
    rankdir=RL;

    node [shape = rectangle];
    Laboratory -> EPD [ label = "1. uploads data" ]
    Patient -> EPD [ label = "2. enables data access for Indimedis"]
    EPD -> "Event Consumer" [ label = "3. new data accessable" ];
    "Event Consumer"  -> vcfreader  [ label = "4. triggers" ];
}
```

Alternativ:

```graphviz
digraph finite_state_machine {
    rankdir=RL;

    node [shape = rectangle];
    Laboratory -> "Event Consumer" [ label = "3. Uploaded File ID=123 of Patient ID=12345"]
    EPD -> "VCF-Reader" [ label = "(5. Request File ID=123 of Patient ID=12345)" ]
    Patient -> EPD [ label = "2. enables data access for Indimedis"]
    Laboratory -> EPD [ label = "1. uploads data" ]
    "Event Consumer" -> "VCF-Reader" [ label = "4. Read File ID=123 of Patient ID=12345" ];
}
```