package genotyper

import "indimedis.com/pgxcdss/pgxreporter/haplotyper"

type Result struct {
	ConfirmedGenotypes []Genotype
	PossibleGenotypes  []Genotype
	ExcludedGenotypes  []Genotype
}

type Genotype struct {
	Haplotypes []haplotyper.Haplotype
}

type Haplotype struct {
	Status int // -1 = Excluded, 0 = Unknown, 1 = Confirmed
}
