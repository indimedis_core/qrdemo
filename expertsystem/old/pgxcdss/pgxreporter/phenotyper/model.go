package phenotyper

import "indimedis.com/pgxcdss/pgxreporter/genotyper"

type Result struct {
	Phenotypes []Phenotype
}

type Phenotype struct {
	Status      int // -1 = Excluded, 0 = Unknown, 1 = Confirmed
	IndicatedBy genotyper.Genotype
}
