module indimedis.com/pgxcdss/pgxreporter/webview/v2

go 1.13

require (
	github.com/dgraph-io/dgo/v2 v2.1.0 // indirect
	github.com/gin-gonic/gin v1.5.0
	github.com/segmentio/kafka-go v0.3.4 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.6.1 // indirect
)
