package main

import (
	"errors"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type Laboratory struct {
	ID           string
	Name         string
	NextAnalyzes string
	Price        string
}

var labs = map[string]Laboratory{
	"1": Laboratory{
		ID:           "1",
		Name:         "Insel",
		NextAnalyzes: "01.01.1001",
		Price:        "500",
	},
	"2": Laboratory{
		ID:           "2",
		Name:         "USZ",
		NextAnalyzes: "01.01.2001",
		Price:        "500",
	},
	"3": Laboratory{
		ID:           "3",
		Name:         "CHUV",
		NextAnalyzes: "01.01.1001",
		Price:        "500",
	},
}

type Order struct {
	ID       string `json:"ID,omitempty"`
	LabID    string `json:"labID,omitempty"`
	SampleID string `json:"sampleID,omitempty"`
}

func main() {
	r := gin.Default()
	r.LoadHTMLGlob("templates/**/*")
	r.Static("/assets", "assets/")
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", labs)
	})
	r.GET("/order/new", func(c *gin.Context) {
		labID := c.Query("lab")
		c.HTML(http.StatusOK, "order/new.html", labs[labID])

	})
	r.POST("/order/confirm", func(c *gin.Context) {
		order := Order{}
		if err := c.ShouldBindJSON(order); err != nil {
			c.HTML(http.StatusOK, "order/confirm.html", labs[order.LabID])
		}
		c.Error(errors.New("could not bind order message"))
	})
	err := r.Run(":9999") // listen and serve on 0.0.0.0:8080
	if err != nil {
		logrus.Fatalf("server.go:72 --> %s", err)
	}
}
