package snper

type SNP struct {
	Status int // -1 = Excluded, 0 = Unknown, 1 = Confirmed
}
