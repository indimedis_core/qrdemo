package vcf42

type DataSet struct {
	Meta *Meta
	Data []Entry
}

type Entry struct {
	Chrom   string
	Pos     string
	ID      string
	Ref     string
	Alt     []string
	Qual    string
	Filter  string
	Info    map[string][]string
	Format  []string
	Samples map[string]Sample
}

type Meta struct {
	FileFormat string
	FileDate   string
	Source     string
	Reference  string
	Phasing    string
	Assembly   string
	Info       []MetaInfoField
	Filter     []MetaFilterField
	Format     []MetaFormatField
	Alt        []MetaAltField
	Contig     []MetaContigField
	Sample     []MetaSampleField
	Pedigree   []MetaPedigreeField
	PedigreeDB string
	SampleIDs  []string
}

func NewMeta() *Meta {
	return &Meta{
		Info:      []MetaInfoField{},
		Filter:    []MetaFilterField{},
		Format:    []MetaFormatField{},
		Alt:       []MetaAltField{},
		Contig:    []MetaContigField{},
		Sample:    []MetaSampleField{},
		Pedigree:  []MetaPedigreeField{},
		SampleIDs: []string{},
	}
}

type Sample map[string]string
type MetaPedigreeField map[string]string

type MetaSampleField struct {
	ID          string
	Genomes     []string
	Mixture     []string
	Description []string
}
type MetaContigField struct {
	ID       string
	Length   string
	Assembly string
	MD5      string
	Species  string
	Taxonomy string
}

type MetaFormatField struct {
	ID          string
	Number      string
	Type        string
	Description string
}

type MetaAltField struct {
	ID          string
	Description string
}

type MetaFilterField struct {
	ID          string
	Description string
}

type MetaInfoField struct {
	ID          string
	Number      string
	Type        string
	Description string
	Source      string
	Version     string
}
