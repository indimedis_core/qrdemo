package vcf42

import "errors"

var ErrVcfVersionNotSupported = errors.New("VCF version not supported")
var ErrRegExpMismatch = errors.New("invalid format, regexp mismatch")
var ErrInvalidFieldNumber = errors.New("invalid number of fields in vcf column header")
var ErrDuplicatedSampleID = errors.New("duplicated sample id")
