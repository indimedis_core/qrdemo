package vcf42

import (
	"bytes"
	"fmt"
	"reflect"
	"strings"
	"testing"
)

func TestNewVcfReader(t *testing.T) {
	reader1 := NewVcfReader(strings.NewReader(""))
	if reader1 == nil {
		t.Error("NewVcfReader for reader1 returned nil")
	}
	reader2 := NewVcfReader(bytes.NewReader([]byte{}))
	if reader2 == nil {
		t.Error("NewVcfReader for reader2 returned nil")
	}
	reader3 := NewVcfReader(bytes.NewBuffer([]byte{}))
	if reader3 == nil {
		t.Error("NewVcfReader for reader3 returned nil")
	}
}

func TestReadMeta(t *testing.T) {
	tests := map[string]string{ // test 1 should fail, ##source line is invalid
		"test1": `##fileformat=VCFv4.2
##fileDate=20090805
##source=myImputationProgramV3.1
##reference=file:///seq/references/1000GenomesPilot-NCBI36.fasta
##contig=<ID=20,length=62435964,assembly=B36,md5=f126cdf8a6e0c7f379d618ff66beb2da,species="Homo sapiens",taxonomy=x>
##phasing=partial
##INFO=<ID=NS,Number=1,Type=Integer,Description="Number of Samples With Data">
##INFO=<ID=DP,Number=1,Type=Integer,Description="Total Depth">
##INFO=<ID=AF,Number=A,Type=Float,Description="Allele Frequency">
##INFO=<ID=AA,Number=1,Type=String,Description="Ancestral Allele">
##INFO=<ID=DB,Number=0,Type=Flag,Description="dbSNP membership, build 129">
##INFO=<ID=H2,Number=0,Type=Flag,Description="HapMap2 membership">
##FILTER=<ID=q10,Description="Quality below 10">
##FILTER=<ID=s50,Description="Less than 50% of samples have data">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##FORMAT=<ID=GQ,Number=1,Type=Integer,Description="Genotype Quality">
##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Read Depth">
##FORMAT=<ID=HQ,Number=2,Type=Integer,Description="Haplotype Quality">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	NA00001	NA00002	NA00003`,
		"test2": `##fileformat=VCFv4.2
##fileDate=20100501
##reference=1000GenomesPilot-NCBI36
##pedigreeDB=mysql://somewhere.org/assembly
##assembly=ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/sv/breakpoint_assemblies.fasta
##INFO=<ID=BKPTID,Number=.,Type=String,Description="ID of the assembled alternate allele in the assembly file">
##INFO=<ID=CIEND,Number=2,Type=Integer,Description="Confidence interval around END for imprecise variants">
##INFO=<ID=CIPOS,Number=2,Type=Integer,Description="Confidence interval around POS for imprecise variants">
##INFO=<ID=END,Number=1,Type=Integer,Description="End position of the variant described in this record">
##INFO=<ID=HOMLEN,Number=.,Type=Integer,Description="Length of base pair identical micro-homology at event breakpoints">
##INFO=<ID=HOMSEQ,Number=.,Type=String,Description="Sequence of base pair identical micro-homology at event breakpoints">
##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Difference in length between REF and ALT alleles">
##INFO=<ID=SVTYPE,Number=1,Type=String,Description="Type of structural variant">
##ALT=<ID=DEL,Description="Deletion">
##ALT=<ID=DEL:ME:ALU,Description="Deletion of ALU element">
##ALT=<ID=DEL:ME:L1,Description="Deletion of L1 element">
##ALT=<ID=DUP,Description="Duplication">
##ALT=<ID=DUP:TANDEM,Description="Tandem Duplication">
##ALT=<ID=INS,Description="Insertion of novel sequence">
##ALT=<ID=INS:ME:ALU,Description="Insertion of ALU element">
##ALT=<ID=INS:ME:L1,Description="Insertion of L1 element">
##ALT=<ID=INV,Description="Inversion">
##ALT=<ID=CNV,Description="Copy number variable region">
##contig=<ID=20,length=62435964,assembly=B36,md5=f126cdf8a6e0c7f379d618ff66beb2da,species="Homo sapiens">
##contig=<ID=GL000234.1,assembly=b37,length=40531>
##SAMPLE=<ID=S_ID,Genomes=G1_ID;G2_ID,Mixture=N1;N2,Description=S1;S2;SK>
##SAMPLE=<ID=S_ID2,Genomes=G1_ID;G2_ID,Mixture=N1;N2,Description=S1;S2;SK>
##PEDIGREE=<Name_0=G0-ID,Name_1=G1-ID,Name_12=GN-ID>
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##FORMAT=<ID=GQ,Number=1,Type=Integer,Description="Genotype quality">
##FORMAT=<ID=CN,Number=1,Type=Integer,Description="Copy number genotype for imprecise events">
##FORMAT=<ID=CNQ,Number=1,Type=Float,Description="Copy number genotype quality for imprecise events">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	NA00001`,
		"test3": `##fileformat=VCFv4.2
##FILTER=<ID=PASS,Description="All filters passed">
##fileDate=2016-06-03
##source=IlluminaPlatinumGenomes, version: 2016-1.0
##reference=hg38
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	T0001`,
		"test4": `##fileformat=VCFv4.1
		##FILTER=<ID=PASS,Description="All filters passed">
##fileDate=2016-06-03
##source=IlluminaPlatinumGenomes, version: 2016-1.0
##reference=hg38
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	T0001`,
		"test5": `##fileformat=VCFv4.2
##contig=<ID=GL000235.1:assembly=b37:length=34474>`,
		"test6": `##fileformat=VCFv4.2
##INFO=<ID=ID,Number=2,Type=typeDescription="description",Version="version">`,
		"test7": `##fileformat=VCFv4.2
##FORMAT=<ID=HQ,Number=Q,Type=Integer,Description="Haplotype Quality">`,
		"test8": `##fileformat=VCFv4.2
##FILTER=<ID=s50Description="">`,
		"test9": `##fileformat=VCFv4.2
##ALT=<ID=CNV,Description=Copy number variable region>`,
		"test10": `##fileformat=VCFv4.2
##SAMPLE=<>`,
		"test11": `##fileformat=VCFv4.2
##PEDIGREE=<ASDF_0=G0-ID>`,
		"test12": `##fileformat=VCFv4.2
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT`,
		"test13": `##fileformat=VCFv4.2
##fileDate=&&&`,
		"test14": `##fileformat=VCFv4.2
##phasing=%%%%`,
		"test15": `##fileformat=VCFv4.2
##pedigreeDB=`,
		"test16": `##fileformat=VCFv4.2
##reference=çç`,
		"test17": `##fileformat=VCFv4.2
##source=IlluminaPlatinumGenomes, version: 2016-1.0`,
		"test18": `##fileformat=VCFv4.2
##assembly=\"`,
		"test19": `##fileformat`,
	}
	expected := map[string]interface{}{
		"test1": &Meta{
			FileFormat: "VCFv4.2",
			FileDate:   "20090805",
			Source:     "myImputationProgramV3.1",
			Reference:  "file:///seq/references/1000GenomesPilot-NCBI36.fasta",
			Contig: []MetaContigField{
				MetaContigField{
					ID:       "20",
					Length:   "62435964",
					Assembly: "B36",
					MD5:      "f126cdf8a6e0c7f379d618ff66beb2da",
					Species:  "Homo sapiens",
					Taxonomy: "x",
				},
			},
			Phasing: "partial",
			Info: []MetaInfoField{
				MetaInfoField{
					ID:          "NS",
					Number:      "1",
					Type:        "Integer",
					Description: "Number of Samples With Data",
				},
				MetaInfoField{
					ID:          "DP",
					Number:      "1",
					Type:        "Integer",
					Description: "Total Depth",
				},
				MetaInfoField{
					ID:          "AF",
					Number:      "A",
					Type:        "Float",
					Description: "Allele Frequency",
				},
				MetaInfoField{
					ID:          "AA",
					Number:      "1",
					Type:        "String",
					Description: "Ancestral Allele",
				},
				MetaInfoField{
					ID:          "DB",
					Number:      "0",
					Type:        "Flag",
					Description: "dbSNP membership, build 129",
				},
				MetaInfoField{
					ID:          "H2",
					Number:      "0",
					Type:        "Flag",
					Description: "HapMap2 membership",
				},
			},
			Filter: []MetaFilterField{
				MetaFilterField{
					ID:          "q10",
					Description: "Quality below 10",
				},
				MetaFilterField{
					ID:          "s50",
					Description: "Less than 50% of samples have data",
				},
			},
			Format: []MetaFormatField{
				MetaFormatField{
					ID:          "GT",
					Number:      "1",
					Type:        "String",
					Description: "Genotype",
				},
				MetaFormatField{
					ID:          "GQ",
					Number:      "1",
					Type:        "Integer",
					Description: "Genotype Quality",
				},
				MetaFormatField{
					ID:          "DP",
					Number:      "1",
					Type:        "Integer",
					Description: "Read Depth",
				},
				MetaFormatField{
					ID:          "HQ",
					Number:      "2",
					Type:        "Integer",
					Description: "Haplotype Quality",
				},
			},
			Alt:      []MetaAltField{},
			Sample:   []MetaSampleField{},
			Pedigree: []MetaPedigreeField{},
			SampleIDs: []string{
				"NA00001", "NA00002", "NA00003",
			},
		},
		"test2": &Meta{
			FileFormat: "VCFv4.2",
			FileDate:   "20100501",
			Reference:  "1000GenomesPilot-NCBI36",
			PedigreeDB: "mysql://somewhere.org/assembly",
			Assembly:   "ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/release/sv/breakpoint_assemblies.fasta",
			Info: []MetaInfoField{
				MetaInfoField{
					ID:          "BKPTID",
					Number:      ".",
					Type:        "String",
					Description: "ID of the assembled alternate allele in the assembly file",
				},
				MetaInfoField{
					ID:          "CIEND",
					Number:      "2",
					Type:        "Integer",
					Description: "Confidence interval around END for imprecise variants",
				},
				MetaInfoField{
					ID:          "CIPOS",
					Number:      "2",
					Type:        "Integer",
					Description: "Confidence interval around POS for imprecise variants",
				},
				MetaInfoField{
					ID:          "END",
					Number:      "1",
					Type:        "Integer",
					Description: "End position of the variant described in this record",
				},
				MetaInfoField{
					ID:          "HOMLEN",
					Number:      ".",
					Type:        "Integer",
					Description: "Length of base pair identical micro-homology at event breakpoints",
				},
				MetaInfoField{
					ID:          "HOMSEQ",
					Number:      ".",
					Type:        "String",
					Description: "Sequence of base pair identical micro-homology at event breakpoints",
				},
				MetaInfoField{
					ID:          "SVLEN",
					Number:      ".",
					Type:        "Integer",
					Description: "Difference in length between REF and ALT alleles",
				},
				MetaInfoField{
					ID:          "SVTYPE",
					Number:      "1",
					Type:        "String",
					Description: "Type of structural variant",
				},
			},
			Alt: []MetaAltField{
				MetaAltField{
					ID:          "DEL",
					Description: "Deletion",
				},
				MetaAltField{
					ID:          "DEL:ME:ALU",
					Description: "Deletion of ALU element",
				},
				MetaAltField{
					ID:          "DEL:ME:L1",
					Description: "Deletion of L1 element",
				},
				MetaAltField{
					ID:          "DUP",
					Description: "Duplication",
				},
				MetaAltField{
					ID:          "DUP:TANDEM",
					Description: "Tandem Duplication",
				},
				MetaAltField{
					ID:          "INS",
					Description: "Insertion of novel sequence",
				},
				MetaAltField{
					ID:          "INS:ME:ALU",
					Description: "Insertion of ALU element",
				},
				MetaAltField{
					ID:          "INS:ME:L1",
					Description: "Insertion of L1 element",
				},
				MetaAltField{
					ID:          "INV",
					Description: "Inversion",
				},
				MetaAltField{
					ID:          "CNV",
					Description: "Copy number variable region",
				},
			},
			Format: []MetaFormatField{
				MetaFormatField{
					ID:          "GT",
					Number:      "1",
					Type:        "String",
					Description: "Genotype",
				},
				MetaFormatField{
					ID:          "GQ",
					Number:      "1",
					Type:        "Integer",
					Description: "Genotype quality",
				},
				MetaFormatField{
					ID:          "CN",
					Number:      "1",
					Type:        "Integer",
					Description: "Copy number genotype for imprecise events",
				},
				MetaFormatField{
					ID:          "CNQ",
					Number:      "1",
					Type:        "Float",
					Description: "Copy number genotype quality for imprecise events",
				},
			},
			Filter: []MetaFilterField{}, // Multiple filters are tested in test2
			Contig: []MetaContigField{
				MetaContigField{
					ID:       "20",
					Length:   "62435964",
					Assembly: "B36",
					MD5:      "f126cdf8a6e0c7f379d618ff66beb2da",
					Species:  "Homo sapiens",
				},
				MetaContigField{
					ID:       "GL000234.1",
					Assembly: "b37",
					Length:   "40531",
				},
			},
			Sample: []MetaSampleField{
				MetaSampleField{
					ID:          "S_ID",
					Genomes:     []string{"G1_ID", "G2_ID"},
					Mixture:     []string{"N1", "N2"},
					Description: []string{"S1", "S2", "SK"},
				},
				MetaSampleField{
					ID:          "S_ID2",
					Genomes:     []string{"G1_ID", "G2_ID"},
					Mixture:     []string{"N1", "N2"},
					Description: []string{"S1", "S2", "SK"},
				},
			},
			Pedigree: []MetaPedigreeField{
				MetaPedigreeField{
					"Name_0":  "G0-ID",
					"Name_1":  "G1-ID",
					"Name_12": "GN-ID",
				},
			},
			SampleIDs: []string{
				"NA00001",
			},
		},
		"test3":  fmt.Errorf("value: %v expression: %v |-> %w", "##source=IlluminaPlatinumGenomes, version: 2016-1.0", SingleKeyValidationExpression, ErrRegExpMismatch),
		"test4":  fmt.Errorf("%v |-> %w", "##fileformat=VCFv4.1", ErrVcfVersionNotSupported),
		"test5":  fmt.Errorf("value: %v expression: %v |-> %w", "##contig=<ID=GL000235.1:assembly=b37:length=34474>", ContigValidationExpression, ErrRegExpMismatch),
		"test6":  fmt.Errorf("value: %v expression: %v |-> %w", "##INFO=<ID=ID,Number=2,Type=typeDescription=\"description\",Version=\"version\">", InfoValidationExpression, ErrRegExpMismatch),
		"test7":  fmt.Errorf("value: %v expression: %v |-> %w", "##FORMAT=<ID=HQ,Number=Q,Type=Integer,Description=\"Haplotype Quality\">", FormatValidationExpression, ErrRegExpMismatch),
		"test8":  fmt.Errorf("value: %v expression: %v |-> %w", "##FILTER=<ID=s50Description=\"\">", FilterValidationExpression, ErrRegExpMismatch),
		"test9":  fmt.Errorf("value: %v expression: %v |-> %w", "##ALT=<ID=CNV,Description=Copy number variable region>", AltValidationExpression, ErrRegExpMismatch),
		"test10": fmt.Errorf("value: %v expression: %v |-> %w", "##SAMPLE=<>", SampleValidationExpression, ErrRegExpMismatch),
		"test11": fmt.Errorf("value: %v expression: %v |-> %w", "##PEDIGREE=<ASDF_0=G0-ID>", PedigreeValidationExpression, ErrRegExpMismatch),
		"test12": fmt.Errorf("value: %v |-> %w", "#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\tFORMAT", ErrInvalidFieldNumber),
		"test13": fmt.Errorf("value: %v expression: %v |-> %w", "##fileDate=&&&", SingleKeyValidationExpression, ErrRegExpMismatch),
		"test14": fmt.Errorf("value: %v expression: %v |-> %w", "##phasing=%%%%", SingleKeyValidationExpression, ErrRegExpMismatch),
		"test15": fmt.Errorf("value: %v expression: %v |-> %w", "##pedigreeDB=", SingleKeyValidationExpression, ErrRegExpMismatch),
		"test16": fmt.Errorf("value: %v expression: %v |-> %w", "##reference=çç", SingleKeyValidationExpression, ErrRegExpMismatch),
		"test17": fmt.Errorf("value: %v expression: %v |-> %w", "##source=IlluminaPlatinumGenomes, version: 2016-1.0", SingleKeyValidationExpression, ErrRegExpMismatch),
		"test18": fmt.Errorf("value: %v expression: %v |-> %w", "##assembly=\\\"", SingleKeyValidationExpression, ErrRegExpMismatch),
		"test19": fmt.Errorf("value: %v expression: %v |-> %w", "##fileformat", SingleKeyValidationExpression, ErrRegExpMismatch),
	}

	for k, v := range tests {
		reader := NewVcfReader(strings.NewReader(v))
		result, err := reader.ReadMeta()
		if !reflect.DeepEqual(expected[k], result) && !reflect.DeepEqual(expected[k], err) {
			t.Errorf("\nexpected result for test case '%v':\n\t%v \nresult: \n\t%v and error: %v", k, expected[k], result, err)
		}
	}
}

func TestSingleKeyLine(t *testing.T) {
	tests := map[string]string{
		"test1": `##fileformat=VCFv4.2`,
		"test2": `##fileDate=20090805`,
		"test3": `##source=myImputationProgramV3.1`,
		"test4": `##reference=file:///seq/references/1000GenomesPilot-NCBI36.fasta`,
		"test5": `##contig=<ID=GL000235.1>`, // not a "single key line" --> should fail
		"test6": `##pedigreeDB=mysql://somewhere.org/assembly`,
	}
	expected := map[string]interface{}{
		"test1": "VCFv4.2",
		"test2": "20090805",
		"test3": "myImputationProgramV3.1",
		"test4": "file:///seq/references/1000GenomesPilot-NCBI36.fasta",
		"test5": fmt.Errorf("value: %v expression: %v |-> %w", tests["test5"], SingleKeyValidationExpression, ErrRegExpMismatch),
		"test6": "mysql://somewhere.org/assembly",
	}

	for k, v := range tests {
		result, err := parseSingleKeyLine(v)
		if !reflect.DeepEqual(expected[k], result) && !reflect.DeepEqual(expected[k], err) {
			t.Errorf("\nexpected result for test case '%v':\n\t%v \nresult: \n\t%v and error: %v", k, expected[k], result, err)
		}
	}
}

func TestParseContig(t *testing.T) {
	tests := map[string]string{
		"test1": `##contig=<ID=20,length=62435964,assembly=B36,md5=f126cdf8a6e0c7f379d618ff66beb2da,species="Homo sapiens",taxonomy=x>`,
		"test2": `##contig=<ID=20,length=62435964,assembly=B36,md5=f126cdf8a6e0c7f379d618ff66beb2da,species="Homo sapiens">`,
		"test3": `##contig=<ID=GL000234.1,assembly=b37,length=40531>`,
		"test4": `##contig=<ID=GL000235.1,assembly=b37,length=34474>`,
		"test5": `##contig=<ID=GL000235.1>`,
		"test6": `##contig=<ID=GL000235.1:assembly=b37:length=34474>`, // : instead of , --> should fail
	}
	expected := map[string]interface{}{
		"test1": &MetaContigField{
			ID:       "20",
			Length:   "62435964",
			Assembly: "B36",
			MD5:      "f126cdf8a6e0c7f379d618ff66beb2da",
			Species:  "Homo sapiens",
			Taxonomy: "x",
		},
		"test2": &MetaContigField{
			ID:       "20",
			Length:   "62435964",
			Assembly: "B36",
			MD5:      "f126cdf8a6e0c7f379d618ff66beb2da",
			Species:  "Homo sapiens",
		},
		"test3": &MetaContigField{
			ID:       "GL000234.1",
			Assembly: "b37",
			Length:   "40531",
		},
		"test4": &MetaContigField{
			ID:       "GL000235.1",
			Length:   "34474",
			Assembly: "b37",
		},
		"test5": &MetaContigField{
			ID: "GL000235.1",
		},
		"test6": fmt.Errorf("value: %v expression: %v |-> %w", tests["test6"], ContigValidationExpression, ErrRegExpMismatch),
	}

	for k, v := range tests {
		result, err := parseContig(v)
		if !reflect.DeepEqual(expected[k], result) && !reflect.DeepEqual(expected[k], err) {
			t.Errorf("\nexpected result for test case '%v':\n\t%v \nresult: \n\t%v and error: %v", k, expected[k], result, err)
		}
	}
}

func TestParseInfo(t *testing.T) {
	tests := map[string]string{
		"test1": `##INFO=<ID=NS,Number=1,Type=Integer,Description="Number of Samples With Data">`,
		"test2": `##INFO=<ID=DB,Number=0,Type=Flag,Description="dbSNP membership, build 129">`,
		"test3": `##INFO=<ID=ID,Number=1,Type=type,Description="description",Source="source",Version="version">`,
		"test4": `##INFO=<ID=ID,Number=2,Type=type,Description="description",Source="source">`,
		"test5": `##INFO=<ID=ID,Number=2,Type=type,Description="description",Version="version">`,
		"test6": `##INFO=<ID=ID,Number=2,Type=typeDescription="description",Version="version">`, // Should fail, missing comma
	}

	expected := map[string]interface{}{
		"test1": &MetaInfoField{
			ID:          "NS",
			Number:      "1",
			Type:        "Integer",
			Description: "Number of Samples With Data",
		},
		"test2": &MetaInfoField{
			ID:          "DB",
			Number:      "0",
			Type:        "Flag",
			Description: "dbSNP membership, build 129",
		},
		"test3": &MetaInfoField{
			ID:          "ID",
			Number:      "1",
			Type:        "type",
			Description: "description",
			Source:      "source",
			Version:     "version",
		},
		"test4": &MetaInfoField{
			ID:          "ID",
			Number:      "2",
			Type:        "type",
			Description: "description",
			Source:      "source",
		},
		"test5": &MetaInfoField{
			ID:          "ID",
			Number:      "2",
			Type:        "type",
			Description: "description",
			Version:     "version",
		},
		"test6": fmt.Errorf("value: %v expression: %v |-> %w", tests["test6"], InfoValidationExpression, ErrRegExpMismatch),
	}

	for k, v := range tests {
		result, err := parseInfo(v)
		if !reflect.DeepEqual(expected[k], result) && !reflect.DeepEqual(expected[k], err) {
			t.Errorf("\nexpected result for test case '%v':\n\t%v \nresult: \n\t%v and error: %v", k, expected[k], result, err)
		}
	}
}

func TestParseFormat(t *testing.T) {
	tests := map[string]string{
		"test1": `##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">`,
		"test2": `##FORMAT=<ID=GQ,Number=1,Type=Integer,Description="Genotype Quality">`,
		"test3": `##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Read Depth">`,
		"test4": `##FORMAT=<ID=HQ,Number=2,Type=Integer,Description="Haplotype Quality">`,
		"test5": ``,
	}
	expected := map[string]interface{}{
		"test1": &MetaFormatField{
			ID:          "GT",
			Number:      "1",
			Type:        "String",
			Description: "Genotype",
		},
		"test2": &MetaFormatField{
			ID:          "GQ",
			Number:      "1",
			Type:        "Integer",
			Description: "Genotype Quality",
		},
		"test3": &MetaFormatField{
			ID:          "DP",
			Number:      "1",
			Type:        "Integer",
			Description: "Read Depth",
		},
		"test4": &MetaFormatField{
			ID:          "HQ",
			Number:      "2",
			Type:        "Integer",
			Description: "Haplotype Quality",
		},
		"test5": fmt.Errorf("value: %v expression: %v |-> %w", tests["test5"], FormatValidationExpression, ErrRegExpMismatch),
	}

	for k, v := range tests {
		result, err := parseFormat(v)
		if !reflect.DeepEqual(expected[k], result) && !reflect.DeepEqual(expected[k], err) {
			t.Errorf("\nexpected result for test case '%v':\n\t%v \nresult: \n\t%v and error: %v", k, expected[k], result, err)
		}
	}
}
func TestParseFilter(t *testing.T) {
	tests := map[string]string{
		"test1": `##FILTER=<ID=q10,Description="Quality below 10">`,
		"test2": `##FILTER=<ID=s50,Description="Less than 50% of samples have data">`,
		"test3": `##FILTER=<ID=s50Description="">`, // missing comma (,)
	}
	expected := map[string]interface{}{
		"test1": &MetaFilterField{
			ID:          "q10",
			Description: "Quality below 10",
		},
		"test2": &MetaFilterField{
			ID:          "s50",
			Description: "Less than 50% of samples have data",
		},
		"test3": fmt.Errorf("value: %v expression: %v |-> %w", tests["test3"], FilterValidationExpression, ErrRegExpMismatch),
	}

	for k, v := range tests {
		result, err := parseFilter(v)
		if !reflect.DeepEqual(expected[k], result) && !reflect.DeepEqual(expected[k], err) {
			t.Errorf("\nexpected result for test case '%v':\n\t%v \nresult: \n\t%v and error: %v", k, expected[k], result, err)
		}
	}

}
func TestParseAlt(t *testing.T) {
	tests := map[string]string{
		"test1":  `##ALT=<ID=DEL,Description="Deletion">`,
		"test2":  `##ALT=<ID=DEL:ME:ALU,Description="Deletion of ALU element">`,
		"test3":  `##ALT=<ID=DEL:ME:L1,Description="Deletion of L1 element">`,
		"test4":  `##ALT=<ID=DUP,Description="Duplication">`,
		"test5":  `##ALT=<ID=DUP:TANDEM,Description="Tandem Duplication">`,
		"test6":  `##ALT=<ID=INS,Description="Insertion of novel sequence">`,
		"test7":  `##ALT=<ID=INS:ME:ALU,Description="Insertion of ALU element">`,
		"test8":  `##ALT=<ID=INS:ME:L1,Description="Insertion of L1 element">`,
		"test9":  `##ALT=<ID=INV,Description="Inversion">`,
		"test10": `##ALT=<ID=CNV,Description="Copy number variable region">`,
		"test11": `##ALT=<ID=CNV,Description=Copy number variable region>`, // missing quotes
		"test12": `##ALT=<ID=CNV>`,                                         // missing description
	}
	expected := map[string]interface{}{
		"test1": &MetaAltField{
			ID:          "DEL",
			Description: "Deletion",
		},
		"test2": &MetaAltField{
			ID:          "DEL:ME:ALU",
			Description: "Deletion of ALU element",
		},
		"test3": &MetaAltField{
			ID:          "DEL:ME:L1",
			Description: "Deletion of L1 element",
		},
		"test4": &MetaAltField{
			ID:          "DUP",
			Description: "Duplication",
		},
		"test5": &MetaAltField{
			ID:          "DUP:TANDEM",
			Description: "Tandem Duplication",
		},
		"test6": &MetaAltField{
			ID:          "INS",
			Description: "Insertion of novel sequence",
		},
		"test7": &MetaAltField{
			ID:          "INS:ME:ALU",
			Description: "Insertion of ALU element",
		},
		"test8": &MetaAltField{
			ID:          "INS:ME:L1",
			Description: "Insertion of L1 element",
		},
		"test9": &MetaAltField{
			ID:          "INV",
			Description: "Inversion",
		},
		"test10": &MetaAltField{
			ID:          "CNV",
			Description: "Copy number variable region",
		},
		"test11": fmt.Errorf("value: %v expression: %v |-> %w", tests["test11"], AltValidationExpression, ErrRegExpMismatch),
		"test12": fmt.Errorf("value: %v expression: %v |-> %w", tests["test12"], AltValidationExpression, ErrRegExpMismatch),
	}

	for k, v := range tests {
		result, err := parseAlt(v)
		if !reflect.DeepEqual(expected[k], result) && !reflect.DeepEqual(expected[k], err) {
			t.Errorf("\nexpected result for test case '%v':\n\t%v \nresult: \n\t%v and error: %v", k, expected[k], result, err)
		}
	}

}

func TestParseSample(t *testing.T) {
	tests := map[string]string{
		"test1": "##SAMPLE=<ID=S_ID,Genomes=G1_ID;G2_ID;GK_ID,Mixture=N1;N2,Description=S1;S2;SK>",
		"test2": "##SAMPLE=<>",
		"test3": "##SAMPLE=<ID=S_ID,Genomes=G1_ID,Mixture=N2,Description=SK>",
	}
	expected := map[string]interface{}{
		"test1": &MetaSampleField{
			ID:          "S_ID",
			Genomes:     []string{"G1_ID", "G2_ID", "GK_ID"},
			Mixture:     []string{"N1", "N2"},
			Description: []string{"S1", "S2", "SK"},
		},
		"test2": fmt.Errorf("value: %v expression: %v |-> %w", tests["test2"], SampleValidationExpression, ErrRegExpMismatch),
		"test3": &MetaSampleField{
			ID:          "S_ID",
			Genomes:     []string{"G1_ID"},
			Mixture:     []string{"N2"},
			Description: []string{"SK"},
		},
	}

	for k, v := range tests {
		result, err := parseSample(v)
		if !reflect.DeepEqual(expected[k], result) && !reflect.DeepEqual(expected[k], err) {
			t.Errorf("\nexpected result for test case '%v':\n\t%v \nresult: \n\t%v and error: %v", k, expected[k], result, err)
		}
	}

}

func TestParsePedigree(t *testing.T) {

	// Regex für v4.3 ^(##PEDIGREE=<)(ID=[\w]+)((,Name_[\d]+=[\w\d-]+)+|,Original=([\w\d-]+)|,Father=([\w\d-]+),Mother=([\w\d-]+))>$
	tests := map[string]string{
		"test1": "##PEDIGREE=<ASDF_0=G0-ID>",
		"test2": "##PEDIGREE=<Name_N=G0-ID>",
		"test3": "##PEDIGREE=<Name_0=G0-ID>",
		"test4": "##PEDIGREE=<Name_0=G0-ID,Name_1=G1-ID,Name_12=GN-ID>",
	}
	expected := map[string]interface{}{
		"test1": fmt.Errorf("value: %v expression: %v |-> %w", tests["test1"], PedigreeValidationExpression, ErrRegExpMismatch),
		"test2": fmt.Errorf("value: %v expression: %v |-> %w", tests["test2"], PedigreeValidationExpression, ErrRegExpMismatch),
		"test3": MetaPedigreeField{"Name_0": "G0-ID"},
		"test4": MetaPedigreeField{
			"Name_0":  "G0-ID",
			"Name_1":  "G1-ID",
			"Name_12": "GN-ID",
		},
	}
	for k, v := range tests {
		result, err := parsePedigree(v)
		if !reflect.DeepEqual(expected[k], result) && !reflect.DeepEqual(expected[k], err) {
			t.Errorf("\nexpected result for test case '%v':\n\t%v \nresult: \n\t%v and error: %v", k, expected[k], result, err)
		}
	}
}

func TestParseSampleIDs(t *testing.T) {
	tests := map[string]string{
		"test1": `#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO`,
		"test2": `#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	TEST_ID`,
		"test3": `#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	Test_0	0	123	asdf	1_ab_AB`,
		"test4": `#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	Test_0	0	123	asdf	0`,
		"test5": `#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT`,
	}
	expected := map[string]interface{}{
		"test1": []string{},
		"test2": []string{"TEST_ID"},
		"test3": []string{
			"Test_0",
			"0",
			"123",
			"asdf",
			"1_ab_AB",
		},
		"test4": fmt.Errorf("value: %v |-> %w", "0", ErrDuplicatedSampleID),
		"test5": fmt.Errorf("value: %v |-> %w", tests["test5"], ErrInvalidFieldNumber),
	}
	for k, v := range tests {
		result, err := parseSampleIDs(v)
		if !reflect.DeepEqual(expected[k], result) && !reflect.DeepEqual(expected[k], err) {
			t.Errorf("\nexpected result for test case '%v':\n\t%v \nresult: \n\t%v and error: %v", k, expected[k], result, err)
		}
	}
}
