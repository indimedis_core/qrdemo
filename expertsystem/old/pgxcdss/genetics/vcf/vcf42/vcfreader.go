package vcf42

import (
	"bufio"
	"fmt"
	"io"
	"regexp"
	"strings"
)

// VcfVersion is the supported VCF version by this package
const VcfVersion = "VCFv4.2"

// MetaFileFormatPrefix is the key '##fileformat'
const MetaFileFormatPrefix = "##fileformat"

// MetaFileDatePrefix is the key '##fileDate'
const MetaFileDatePrefix = "##fileDate"

// MetaSourcePrefix is the key '##source'
const MetaSourcePrefix = "##source"

// MetaReferencePrefix is the key '##reference'
const MetaReferencePrefix = "##reference"

// MetaAssemblyPrefix is the key '##assembly'
const MetaAssemblyPrefix = "##assembly"

// MetaContigPrefix is the key '##contig'
const MetaContigPrefix = "##contig"

// MetaPhasingPrefix is the key '##phasing'
const MetaPhasingPrefix = "##phasing"

// MetaInfoPrefix is the key '##INFO'
const MetaInfoPrefix = "##INFO"

// MetaFormatPrefix is the key '##FORMAT'
const MetaFormatPrefix = "##FORMAT"

// MetaFilterPrefix is the key '##FILTER'
const MetaFilterPrefix = "##FILTER"

// MetaAltPrefix is the key '##ALT'
const MetaAltPrefix = "##ALT"

// MetaSamplePrefix is the key '##SAMPLE'
const MetaSamplePrefix = "##SAMPLE"

// MetaPedigreePrefix is the key '##PEDIGREE'
const MetaPedigreePrefix = "##PEDIGREE"

// MetaPedigreeDbPrefix is the key '##pedigreeDB'
const MetaPedigreeDbPrefix = "##pedigreeDB"

// VcfColumnHeaderPrefix is the key '"#CHROM'
const VcfColumnHeaderPrefix = "#CHROM"

type VcfReader struct {
	scanner      *bufio.Scanner
	maxHaplotype int
	meta         *Meta
}

// NewVcfReader creates an new vcfReader instance
func NewVcfReader(r io.Reader) *VcfReader {
	vcfreader := &VcfReader{
		scanner:      bufio.NewScanner(r),
		maxHaplotype: 2,
	}
	vcfreader.scanner.Split(bufio.ScanLines)
	return vcfreader
}

// ReadMeta reads the meta data from a vcf file
func (r *VcfReader) ReadMeta() (*Meta, error) {
	meta := NewMeta()
	for r.scanner.Scan() {
		v := strings.TrimSpace(r.scanner.Text())
		if strings.HasPrefix(v, MetaFileFormatPrefix) {
			v2, err := parseSingleKeyLine(v)
			if err != nil {
				return nil, err
			}
			if v2 != VcfVersion {
				return nil, fmt.Errorf("%v |-> %w", v, ErrVcfVersionNotSupported)
			}
			meta.FileFormat = v2

		} else if strings.HasPrefix(v, MetaFileDatePrefix) {
			v2, err := parseSingleKeyLine(v)
			if err != nil {
				return nil, err
			}
			meta.FileDate = v2

		} else if strings.HasPrefix(v, MetaSourcePrefix) {
			v2, err := parseSingleKeyLine(v)
			if err != nil {
				return nil, err
			}
			meta.Source = v2

		} else if strings.HasPrefix(v, MetaReferencePrefix) {
			v2, err := parseSingleKeyLine(v)
			if err != nil {
				return nil, err
			}
			meta.Reference = v2

		} else if strings.HasPrefix(v, MetaAssemblyPrefix) {
			v2, err := parseSingleKeyLine(v)
			if err != nil {
				return nil, err
			}
			meta.Assembly = v2

		} else if strings.HasPrefix(v, MetaContigPrefix) {
			contig, err := parseContig(v)
			if err != nil {
				return nil, err
			}
			meta.Contig = append(meta.Contig, *contig)

		} else if strings.HasPrefix(v, MetaPhasingPrefix) {
			v2, err := parseSingleKeyLine(v)
			if err != nil {
				return nil, err
			}
			meta.Phasing = v2

		} else if strings.HasPrefix(v, MetaInfoPrefix) {
			info, err := parseInfo(v)
			if err != nil {
				return nil, err
			}
			meta.Info = append(meta.Info, *info)

		} else if strings.HasPrefix(v, MetaFormatPrefix) {
			format, err := parseFormat(v)
			if err != nil {
				return nil, err
			}
			meta.Format = append(meta.Format, *format)

		} else if strings.HasPrefix(v, MetaFilterPrefix) {
			filter, err := parseFilter(v)
			if err != nil {
				return nil, err
			}
			meta.Filter = append(meta.Filter, *filter)

		} else if strings.HasPrefix(v, MetaAltPrefix) {
			alt, err := parseAlt(v)
			if err != nil {
				return nil, err
			}
			meta.Alt = append(meta.Alt, *alt)

		} else if strings.HasPrefix(v, MetaSamplePrefix) {
			sample, err := parseSample(v)
			if err != nil {
				return nil, err
			}
			meta.Sample = append(meta.Sample, *sample)

		} else if strings.HasPrefix(v, MetaPedigreePrefix) {
			p, err := parsePedigree(v)
			if err != nil {
				return nil, err
			}
			meta.Pedigree = append(meta.Pedigree, p)

		} else if strings.HasPrefix(v, MetaPedigreeDbPrefix) {
			v2, err := parseSingleKeyLine(v)
			if err != nil {
				return nil, err
			}
			meta.PedigreeDB = v2

		} else if strings.HasPrefix(v, VcfColumnHeaderPrefix) {
			ids, err := parseSampleIDs(v)
			if err != nil {
				return nil, err
			}
			meta.SampleIDs = ids
		}

	}
	r.meta = meta
	return r.meta, nil
}

func (r *VcfReader) NextGenotype() (*Entry, error) {
	if r.meta == nil {
		meta, err := r.ReadMeta()
		if err != nil {
			return nil, err
		}
		r.meta = meta
	}
	entry := &Entry{}

	return entry, nil
}

func (r *VcfReader) SetMaxHaplotype(max int) *VcfReader {
	r.maxHaplotype = max
	return r
}

// SingleKeyValidationExpression is the regular expression used to check the format
// of simple key-value pair meta data lines like '##fileformat=VCFv4.2'.
// Value: '^##\w+=[\w-:\/\._;]+$'
const SingleKeyValidationExpression = `^##\w+=[\w-:\/\._;]+$`

// parseSingleKeyLine parses simple key-value pair meta data from vcf files
func parseSingleKeyLine(v string) (string, error) {
	if err := checkFormat(v, SingleKeyValidationExpression); err != nil {
		return "", err
	}
	return strings.Split(v, "=")[1], nil // if validation expression matches the size of the array must be 2 after splitting
}

// ContigValidationExpression is the regular expression used to check the format
// of simple key-value pair meta data lines like '##fileformat=VCFv4.2'.
// Value: '^##contig=<\w+=(([\w-:\._;]+)|"([^"]+)")(,\w+=(([\w-:\._;]+)|"([^"]+)"))*>$'
const ContigValidationExpression = `^##contig=<\w+=(([\w-:\._;]+)|"([^"]+)")(,\w+=(([\w-:\._;]+)|"([^"]+)"))*>$`

// parseContig parses contig meta data from vcf files
func parseContig(v string) (*MetaContigField, error) {

	if err := checkFormat(v, ContigValidationExpression); err != nil {
		return nil, err
	}
	mapKV := splitMetaKeyValues(v)
	return &MetaContigField{
		ID:       mapKV["ID"],
		Length:   mapKV["length"],
		Assembly: mapKV["assembly"],
		MD5:      mapKV["md5"],
		Species:  mapKV["species"],
		Taxonomy: mapKV["taxonomy"],
	}, nil
}

// InfoValidationExpression is the regular expression used to check the format
// of INFO meta data lines.
// Value: '^##INFO=<ID=[\w:]+,Number=[\d\.ARG],Type=[\w]+,Description="[^"]*"(,Source="[^"]*")?(,Version="[^"]*")?>$'
const InfoValidationExpression = `^##INFO=<ID=[\w:]+,Number=[\d\.ARG],Type=[\w]+,Description="[^"]*"(,Source="[^"]*")?(,Version="[^"]*")?>$`

// parseInfo parses info meta data from vcf files
func parseInfo(v string) (*MetaInfoField, error) {
	if err := checkFormat(v, InfoValidationExpression); err != nil {
		return nil, err
	}
	mapKV := splitMetaKeyValues(v)
	return &MetaInfoField{
		ID:          mapKV["ID"],
		Number:      mapKV["Number"],
		Type:        mapKV["Type"],
		Description: mapKV["Description"],
		Source:      mapKV["Source"],
		Version:     mapKV["Version"],
	}, nil
}

// FormatValidationExpression is the regular expression used to check the format
// of FORMAT meta data lines.
// Value: '^##FORMAT=<ID=[\w:]+,Number=[\d\.ARG],Type=[\w]+,Description="[^"]*">$'
const FormatValidationExpression = `^##FORMAT=<ID=[\w:]+,Number=[\d\.ARG],Type=[\w]+,Description="[^"]*">$`

// parseFormat parses format meta data from vcf files
func parseFormat(v string) (*MetaFormatField, error) {

	if err := checkFormat(v, FormatValidationExpression); err != nil {
		return nil, err
	}
	mapKV := splitMetaKeyValues(v)
	return &MetaFormatField{
		ID:          mapKV["ID"],
		Number:      mapKV["Number"],
		Type:        mapKV["Type"],
		Description: mapKV["Description"],
	}, nil
}

// FilterValidationExpression is the regular expression used to check the format
// of FILTER meta data lines.
// Value: '^##FILTER=<ID=[\w:]+,Description="[^"]*">$'
const FilterValidationExpression = `^##FILTER=<ID=[\w:]+,Description="[^"]*">$`

// parseFilter parses filter meta data from vcf files
func parseFilter(v string) (*MetaFilterField, error) {
	if err := checkFormat(v, FilterValidationExpression); err != nil {
		return nil, err
	}
	mapKV := splitMetaKeyValues(v)
	return &MetaFilterField{
		ID:          mapKV["ID"],
		Description: mapKV["Description"],
	}, nil
}

// AltValidationExpression is the regular expression used to check the format
// of ALT meta data lines.
// Value: '^##ALT=<ID=[\w:]+,Description="[^"]*">$'
const AltValidationExpression = `^##ALT=<ID=[\w:]+,Description="[^"]*">$`

// parseAlt parses alt meta data from vcf files
func parseAlt(v string) (*MetaAltField, error) {
	if err := checkFormat(v, AltValidationExpression); err != nil {
		return nil, err
	}
	mapKV := splitMetaKeyValues(v)
	return &MetaAltField{
		ID:          mapKV["ID"],
		Description: mapKV["Description"],
	}, nil
}

// SampleValidationExpression is the regular expression used to check the format
// of SAMPLE meta data lines.
// Value: '^##SAMPLE=<ID=[\w-_]+(,Genomes=([\w]+;)*[\w]+)?(,Mixture=([\w]+;)*[\w]+)?(,Description=([\w]+;)*[\w]+)?>$'
const SampleValidationExpression = `^##SAMPLE=<ID=[\w-_]+(,Genomes=([\w]+;)*[\w]+)?(,Mixture=([\w]+;)*[\w]+)?(,Description=([\w]+;)*[\w]+)?>$`

// parseSample parses sample meta data from vcf files
func parseSample(v string) (*MetaSampleField, error) {
	if err := checkFormat(v, SampleValidationExpression); err != nil {
		return nil, err
	}
	mapKV := splitMetaKeyValues(v)
	return &MetaSampleField{
		ID:          mapKV["ID"],
		Genomes:     strings.Split(mapKV["Genomes"], ";"),
		Mixture:     strings.Split(mapKV["Mixture"], ";"),
		Description: strings.Split(mapKV["Description"], ";"),
	}, nil
}

// PedigreeValidationExpression is the regular expression used to check the format
// of PEDIGREE meta data lines. Note: for v4.3 use
// '^(##PEDIGREE=<)(ID=[\w]+)((,Name_[\d]+=[\w\d-]+)+|,Original=([\w\d-]+)|,Father=([\w\d-]+),Mother=([\w\d-]+))>$'
// Value: '^(##PEDIGREE=<){1}(Name_[\d]+=([\w-]+,))*(Name_[\d]+=[\w-]+>){1}$'
const PedigreeValidationExpression = `^(##PEDIGREE=<){1}(Name_[\d]+=([\w-]+,))*(Name_[\d]+=[\w-]+>){1}$`

// parsePedigree parses pedigree meta data from vcf files
func parsePedigree(v string) (MetaPedigreeField, error) {
	if err := checkFormat(v, PedigreeValidationExpression); err != nil {
		return nil, err
	}
	mapKV := splitMetaKeyValues(v)
	return mapKV, nil
}

// parseSampleIDs extracts the sample ids from the VCF Header line (#CHROM...)
func parseSampleIDs(v string) ([]string, error) {
	fields := strings.Split(v, "\t")
	if len(fields) == 9 {
		return nil, fmt.Errorf("value: %v |-> %w", v, ErrInvalidFieldNumber)
	}
	sampleIDs := map[string]interface{}{}
	result := []string{}
	for i := 9; i < len(fields); i++ {
		if _, ok := sampleIDs[fields[i]]; ok {
			return nil, fmt.Errorf("value: %v |-> %w", fields[i], ErrDuplicatedSampleID)
		}
		sampleIDs[fields[i]] = nil
		result = append(result, fields[i])
	}
	return result, nil
}

func checkFormat(line string, regex string) error {
	expr := regexp.MustCompile(regex)
	if !expr.MatchString(line) {
		return fmt.Errorf("value: %v expression: %v |-> %w", line, regex, ErrRegExpMismatch)
	}
	return nil
}

func splitMetaKeyValues(line string) map[string]string {
	pattern := regexp.MustCompile(`\w+=(([\w-:._;]+)|"([^"]+)")`)
	kv := pattern.FindAllString(line, -1)
	res := map[string]string{}
	for _, v := range kv {
		v = strings.ReplaceAll(v, "\"", "")
		tmp := strings.Split(v, "=")
		res[tmp[0]] = tmp[1]
	}
	return res
}

// func lazyQuoteSplit(s string, sep rune) ([]string, error) {
// 	r := csv.NewReader(strings.NewReader(s))
// 	r.Comma = sep
// 	r.LazyQuotes = true
// 	result, err := r.Read()
// 	if err != nil {
// 		return nil, err
// 	}
// 	if len(result) != 1 {
// 		return nil, errors.New("vcfreader.go#splitByTab is not for multi-line data")
// 	}
// 	return result, nil
// }
