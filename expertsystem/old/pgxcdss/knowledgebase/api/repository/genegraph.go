package repository

import (
	"context"
	"encoding/json"
	"log"

	"github.com/dgraph-io/dgo/v2/protos/api"
	"indimedis.com/fhir/app/genegraph/model"
	"indimedis.com/fhir/pkg/dgraph"
)

type GeneGraphRepository struct {
	db dgraph.Client
	// json jsoniter.API
}

func NewGeneGraphRepo(client dgraph.Client) *GeneGraphRepository {
	// , jsonApi jsoniter.API
	return &GeneGraphRepository{
		db: client,
		// json: jsonApi,
	}
}

func (r *GeneGraphRepository) Create(p model.GeneGraph) {
	conn, closeFunc := r.db.Open()
	txn := conn.NewTxn()
	var x = make([]interface{}, 0)

	for _, v := range p.Assemblies {
		x = append(x, v)
	}
	for _, v := range p.Sequences {
		x = append(x, v)
	}
	for _, v := range p.Snps {
		x = append(x, v)
	}
	for _, v := range p.Haplotypes {
		x = append(x, v)
	}
	for _, v := range p.Genotypes {
		x = append(x, v)
	}
	for _, v := range p.Phenotypes {
		x = append(x, v)
	}

	b, err := json.MarshalIndent(x, "", "  ")
	log.Println(string(b))
	if err != nil {
		log.Fatal(err)
	}
	mu := &api.Mutation{
		SetJson: b,
	}
	resp, err := txn.Mutate(context.Background(), mu)
	if err != nil {
		log.Fatal(err, resp)
	}

	err = txn.Commit(context.Background())
	if err != nil {
		log.Fatal(err, resp)
	}
	log.Println(resp.String())

	closeFunc()
}
