package repository_test

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"testing"
	"time"

	"github.com/dgraph-io/dgo/v2/protos/api"
	"indimedis.com/fhir/app/genegraph"
	"indimedis.com/fhir/app/genegraph/model"
	"indimedis.com/fhir/pkg/dgraph"
)

var conf *genegraph.Configuration
var dClient dgraph.Client
var data model.GeneGraph

func TestMain(m *testing.M) {
	var exitVal int
	defer func() {
		// b, err := exec.Command("docker-compose", "-f", "./testdata/docker-compose.yaml", "down", "-v").CombinedOutput()
		// if err != nil {
		// 	log.Println(err)
		// }
		// log.Println(string(b))
		//	cleanGraphDB()
		os.Exit(exitVal)
	}()

	log.Println("Do stuff BEFORE the tests!")
	out, err := exec.Command("docker-compose", "-f", "./testdata/docker-compose.yaml", "up", "-d").CombinedOutput()
	if err != nil {
		log.Fatalln(err, string(out))
	}
	log.Println(string(out))
	for i := 3; i > 0; i-- {
		log.Println("Waiting for Dgraph...", i)
		time.Sleep(1 * time.Second)
	}

	conf = genegraph.NewConfiguration()
	dClient = dgraph.NewClient(conf.GetDgraphServers())
	cleanGraphDB()
	err = dClient.InitGraph(genegraph.Schema)
	if err != nil {
		log.Fatalln(err)
	}
	data = loadAssemblies("testdata/genegraph.json")

	exitVal = m.Run()

	log.Println("Do stuff AFTER the tests!")
}

func cleanGraphDB() {
	c, closeFunc := dClient.Open()
	defer closeFunc()

	op := &api.Operation{}
	op.DropAll = true
	c.Alter(context.Background(), op)
}

func loadAssemblies(path string) model.GeneGraph {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		log.Fatal(err)
	}
	tmp := model.GeneGraph{}
	err = json.Unmarshal(b, &tmp)
	if err != nil {
		log.Fatal(err)
	}
	return tmp
}
