# Input
```
date: 101020
ref: grch37
snps:
  - [1, 1234, A, B, 0/1] // Chr, Pos, Alt, Ref, DP
  - [1, 1234, A, B, 1/1]
```
Minify nutzen. ```{ref: grch37, vars: [[1, 1234, A, B, 0/1], [1, 1234, A, B, 1/1]]}```

Für den QR-Code den Input gzippen und mit Base64 codieren. Scheint effizienter zu sein als mit Base36 (Alphanumeric QR-Code), was überraschend ist. (Respektive was man wohl berechnen könnte, was ich aber nicht gemacht habe)
