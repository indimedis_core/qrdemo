package pgxqr

import (
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

type VariantReport struct {
	ref  string
	date string
	vars []Variant
}

type Variant struct {
	chr string
	pos int
	ref string
	alt []string
	gt  []int
}

func (vr VariantReport) Equals(vr2 VariantReport) bool {
	if vr.ref != vr2.ref ||
		vr.date != vr2.date {
		return false
	}
	for i := 0; i < len(vr.vars); i++ {
		if !vr.vars[i].Equals(vr2.vars[i]) {
			return false
		}
	}
	return true
}

func (v Variant) Equals(v2 Variant) bool {
	if v.chr != v2.chr ||
		v.pos != v2.pos ||
		v.ref != v2.ref {
		return false
	}
	if len(v.alt) != len(v2.alt) {
		return false
	}
	if len(v.gt) != len(v2.gt) {
		return false
	}

	for i := 0; i < len(v.alt); i++ {
		if v.alt[i] != v2.alt[i] {
			return false
		}
	}
	for i := 0; i < len(v.gt); i++ {
		if v.gt[i] != v2.gt[i] {
			return false
		}
	}
	return true
}

func Unmarshal(data []byte, v *VariantReport) error {

	struc := struct {
		Ref  string     `yaml:"ref"`
		Date string     `yaml:"date"`
		Vars [][]string `yaml:"vars"`
	}{}
	err := yaml.Unmarshal(data, &struc)
	if err != nil {
		return err
	}
	logrus.Printf("Read YAML data to unmarshal variant report: %s", struc)

	v.ref = struc.Ref
	v.date = struc.Date
	v.vars = make([]Variant, len(struc.Vars))

	for i := 0; i < len(struc.Vars); i++ {
		variant := struc.Vars[i]
		chr := variant[0]
		pos, err := strconv.Atoi(variant[1])
		if err != nil {
			return err
		}
		ref := variant[2]
		alt := strings.Split(variant[3], "/")
		strGT := strings.Split(variant[4], "/")
		gt := make([]int, len(strGT))
		for j := 0; j < len(strGT); j++ {
			gt[j], err = strconv.Atoi(strGT[j])
			if err != nil {
				return err
			}
		}

		v.vars[i] = Variant{
			chr: chr,
			pos: pos,
			ref: ref,
			alt: alt,
			gt:  gt,
		}
	}
	return nil
}

func (vr *VariantReport) Ref() string {
	return vr.ref
}

func (vr *VariantReport) Date() string {
	return vr.date
}

func (vr *VariantReport) Variants() []Variant {
	return vr.vars
}

func (v *Variant) Chr() string {
	return v.chr
}
func (v *Variant) Pos() int {
	return v.pos
}
func (v *Variant) Ref() string {
	return v.ref
}
func (v *Variant) Alt() []string {
	return v.alt
}
func (v *Variant) Gt() []int {
	return v.gt
}
