package pgxqr

import (
	"testing"

	"github.com/sirupsen/logrus"
)

var testYamlReport = []byte(`
date: "101020"
ref: "grch37"
vars:
- [1, 1234, A, B/C, 0/1]
- [1, 1234, A, B/C, 1/1]
`)

var testVariant1 = Variant{
	chr: "1",
	pos: 1234,
	ref: "A",
	alt: []string{"B", "C"},
	gt:  []int{0, 1},
}

var testVariant2 = Variant{
	chr: "1",
	pos: 1234,
	ref: "A",
	alt: []string{"B", "C"},
	gt:  []int{1, 1},
}

var testVariantReport1 = VariantReport{
	ref:  "GRCh38",
	date: "101020",
	vars: []Variant{
		testVariant1,
		testVariant1,
	},
}

var expectedVariantReport = VariantReport{
	ref:  "GRCh38",
	date: "101020",
	vars: []Variant{
		testVariant1,
		testVariant2,
	},
}

func TestVariantReportEquals(t *testing.T) {
	if !expectedVariantReport.Equals(expectedVariantReport) {
		logrus.Error("Equal VariantReports are unequal: expectedVariantReport != expectedVariantReport")
		t.FailNow()
	}
	if expectedVariantReport.Equals(testVariantReport1) {
		logrus.Error("Unequal VariantReports are equal: expectedVariantReport == testVariantReport1")
		t.FailNow()
	}
}

func TestVariantEquals(t *testing.T) {
	if !testVariant1.Equals(testVariant1) {
		logrus.Error("Equal Variants are unequal: testVariant1 != testVariant1")
		t.FailNow()
	}
	if testVariant1.Equals(testVariant2) {
		logrus.Error("Unequal Variants are equal: testVariant1 == testVariant2")
		t.FailNow()
	}
}

func TestUnmarshal(t *testing.T) {
	v := VariantReport{}
	err := Unmarshal(testYamlReport, &v)
	if err != nil {
		logrus.Error(err)
		t.FailNow()
	}
	if !v.Equals(expectedVariantReport) {
		logrus.Error("The read yaml data did not match the expectedVariantReport")
		t.FailNow()
	}
}
